/*** Public Methods ***/
function init() {
	initAPI1_0();
}

/*** Private Methods ***/
function addListeners() {
	document.getElementById("apiDocsItem").addEventListener("click", function() {
		var $el = document.getElementById("apiDocsMenu");
		if($el.style.display == "none" || !$el.style.display) {
			$el.style.display = "block";
		}
		else {
			$el.style.display = "none";
		}
	});

	document.getElementById("apiLvl1_0Item").addEventListener("click", function() {
		var $el = document.getElementById("contentPanel");
		var html = $.get("Plugins/BlowFish/blowFish1_0.html", function(data) {
			$el.innerHTML = data;
			initAPI1_0();
		});
	});

	document.getElementById("searchBar").addEventListener("focus", function() {
		if(this.value == "Search BlowFish...") {
			this.value = "";
		}
		this.style.color = "#000000";
	});

	document.getElementById("searchBar").addEventListener("blur", function() {
		if(this.value == "") {
			this.value = "Search BlowFish...";
			this.style.color = "#AAAAAA";
		}
	});

	var $elements = document.getElementsByClassName("NodeTitle");
	for(var i=0;i<$elements.length;i+=1) {
		var $e = $elements[i];
		$e.addEventListener("click", function() {
			if(this.children[0].innerHTML.indexOf("+") != -1) {
				this.children[0].innerHTML = "<font color='#000000'>-</font>";
			}
			else {
				this.children[0].innerHTML = "<font color='#000000'>+</font>";
			}

			for(var j=1;j<this.parentNode.children.length;j+=1) {
				var $el = this.parentNode.children[j];
				if($el.style.display == "none" || !$el.style.display) {
					$el.style.display = "block";
				}
				else {
					$el.style.display = "none";
				}
			}
		});
	}
}

function initAPI1_0() {
	var $elements = document.getElementsByClassName("CollapseableTitle");
	for(var i=0;i<$elements.length;i+=1) {
		var $e = $elements[i];
		$e.addEventListener("click", function() {
			var $el = this.nextSibling.nextSibling;
			if($el.style.display == "block" || !$el.style.display) {
				$el.style.display = "none";
			}
			else {
				$el.style.display = "block";
			}
		});
		if(i != 0) {
			$e.nextSibling.nextSibling.style.display = "none";
		}
	}
}