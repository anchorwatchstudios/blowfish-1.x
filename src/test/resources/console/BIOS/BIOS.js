/* 
 * BlowFish BIOS v1.0
 */

function main(BlowFish) {

	/*** Private Variables ***/
	var instance = this;
	instance.BLOWFISH = BlowFish;
	instance.ROOT = "BIOS/";
	instance.WIDTH = 853;
	instance.HEIGHT = 480;
	instance.FPS = 60;
	instance.IMAGE_ROOT = instance.ROOT + "Images/";
	instance.AUDIO_ROOT = instance.ROOT + "Audio/";
	instance.GAMES_ROOT = instance.ROOT + "Games/";
	instance.GAME_SPEED = 1;
	instance.MENU = 220;
	instance.START = 13;
	instance.SELECT = 16;
	instance.UP = 38;
	instance.DOWN = 40;
	instance.LEFT = 37;
	instance.RIGHT = 39;
	instance.A = 70;
	instance.B = 68;
	instance.C = 83;
	instance.D = 65;

	instance.resources = new BlowFish.Resources();
	var SPRITE_LOGO = resources.addSprite(instance.IMAGE_ROOT + "BlowFish_392x392.png");
	var SPRITE_FINGER = resources.addSprite(instance.IMAGE_ROOT + "finger.png");
	var SOUND_FINGER = resources.addSound(instance.AUDIO_ROOT + "finger.mp3", false);
	instance.resources.getSound(SOUND_FINGER).volume = .1;

	/*** Public Variables ***/
	instance.urlExists = urlExists;
	instance.init = init;
	instance.startGame = startGame;
	instance.stopGame = stopGame;
	instance.Background = new Background();
	instance.Finger = new Finger(500, 165);
	instance.init();

	/*** Private Methods ***/
	function init() {
		instance.BLOWFISH.setTargetFPS(instance.FPS);
		instance.BLOWFISH.setTargetGameSpeed(instance.GAME_SPEED);
		instance.BLOWFISH.setCanvasWidth(853);
		instance.BLOWFISH.setCanvasHeight(480);
		instance.BLOWFISH.BIOS = instance;
		instance.BLOWFISH.setRoom(new MainMenu());
	}

	function startGame(path) {
		$.getScript(path, function() {
			main(instance.BLOWFISH);
		});
	}

	function stopGame() {
		instance.BLOWFISH.setRoom(new MainMenu());
	}

	function urlExists(url) {
		var bool = false;
		$.ajax({
			url: url,
			async: false,
			success: function() {bool = true;},
		});
		return bool;
	}

	function componentToHex(c) {
		var hex = c.toString(16);
		return hex.length == 1 ? "0" + hex : hex;
	}

	function rgbToHex(r, g, b) {
		return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
	}

	function incrementColor(color, object) {
		var chars = color.split("");
		var r = parseInt(chars[1] + chars[2], 16);
		var g = parseInt(chars[3] + chars[4], 16);
		var b = parseInt(chars[5] + chars[6], 16);
		if(r+object.redSpeed > 0 && r+object.redSpeed <= 255) {
			r += object.redSpeed;
		}
		else {
			object.redSpeed = -object.redSpeed;
			r += object.redSpeed;
		}

		if(g+object.greenSpeed > 0 && g+object.greenSpeed <= 255) {
			g += object.greenSpeed;
		}
		else {
			object.greenSpeed = -object.greenSpeed;
			g += object.greenSpeed;
		}

		if(b+object.blueSpeed > 0 && b+object.blueSpeed <= 255) {
			b += object.blueSpeed;
		}
		else {
			object.blueSpeed = -object.blueSpeed;
			b += object.blueSpeed;
		}
		return rgbToHex(r,g,b);
	}

	/*** Private Classes ***/
	//Rooms
	function MainMenu() {
		inst = new BlowFish.Room(instance.WIDTH, instance.HEIGHT);
		inst.id = "MainMenu"
		inst.add(instance.Background);
		inst.redSpeed = 1;
		inst.greenSpeed = 2;
		inst.blueSpeed = 3;
		inst.add(new Logo(50, 50));
		inst.add(instance.Finger);

		/*** Public Methods ***/
		inst.onDraw = function(BlowFish) {
			var BlowFishG2D = BlowFish.getContext();
			inst.GameObjects[0].onDraw(BlowFish);

			BlowFishG2D.fillStyle = "rgba(0,0,0,0.5)";
    			BlowFishG2D.beginPath();
			var x = 528;
			var y = 143;
			var w = 250;
			var h = 200;
			var r = 30;
  			BlowFishG2D.beginPath();
  			BlowFishG2D.moveTo(x+r, y);
  			BlowFishG2D.arcTo(x+w, y,   x+w, y+h, r);
  			BlowFishG2D.arcTo(x+w, y+h, x,   y+h, r);
  			BlowFishG2D.arcTo(x,   y+h, x,   y,   r);
  			BlowFishG2D.arcTo(x,   y,   x+w, y,   r);
			BlowFishG2D.fill();
  			BlowFishG2D.closePath();

			BlowFishG2D.fillStyle = "#ffffff";
			BlowFishG2D.font = 'bold 32pt "Comic Sans MS"';
			BlowFishG2D.fillText("Games", 590, 206);
			BlowFishG2D.fillText("Options", 575, 256);
			BlowFishG2D.fillText("About", 592, 306);

			for(var i=1;i<inst.GameObjects.length;i+=1) {
				inst.GameObjects[i].onDraw(BlowFish);
			}
		}
		return inst;
	}

	function GamesMenu() {
		inst = new BlowFish.Room(instance.WIDTH, instance.HEIGHT);
		inst.id = "GamesMenu";
		inst.add(instance.Background);
		inst.add(instance.Finger);
		return inst;
	}

	function OptionsMenu() {
		inst = new BlowFish.Room(instance.WIDTH, instance.HEIGHT);
		inst.id = "OptionsMenu";
		inst.add(instance.Background);
		inst.add(instance.Finger);
		inst.add(new Logo(50, 50));

		inst.onDraw = function(BlowFish) {
			var BlowFishG2D = BlowFish.getContext();
			inst.GameObjects[0].onDraw(BlowFish);

			BlowFishG2D.fillStyle = "rgba(0,0,0,0.5)";
    			BlowFishG2D.beginPath();
			var x = 500;
			var y = 35;
			var w = 300;
			var h = 400;
			var r = 30;
  			BlowFishG2D.beginPath();
  			BlowFishG2D.moveTo(x+r, y);
  			BlowFishG2D.arcTo(x+w, y,   x+w, y+h, r);
  			BlowFishG2D.arcTo(x+w, y+h, x,   y+h, r);
  			BlowFishG2D.arcTo(x,   y+h, x,   y,   r);
  			BlowFishG2D.arcTo(x,   y,   x+w, y,   r);
			BlowFishG2D.fill();
  			BlowFishG2D.closePath();

			for(var i=1;i<inst.GameObjects.length;i+=1) {
				inst.GameObjects[i].onDraw(BlowFish);
			}
		}

		return inst;
	}

	function AboutMenu() {
		inst = new BlowFish.Room(instance.WIDTH, instance.HEIGHT);
		inst.id = "AboutMenu";
		inst.add(instance.Background);
		inst.add(instance.Finger);
		inst.add(new Logo(50, 50));
		return inst;
	}

	//Objects
	function Logo(x, y) {
		/*** Private Variables ***/
		var inst = new BlowFish.GameObject(instance.resources.getSprite(SPRITE_LOGO), x, y, 0, 392, 392);
		inst.class = "Logo";
		return inst;
	}

	function Finger(x, y) {
		/*** Private Variables ***/
		var inst = new BlowFish.GameObject(instance.resources.getSprite(SPRITE_FINGER), x, y, 0, 64, 64);

		/*** Public Variables ***/
		inst.class = "Finger";
		inst.xSpeed = 1;
		inst.selection = 0;
		inst.floatX = 500;
		inst.throttle = 1000/15;
		inst.lastUpdate = new Date().getTime();

		/*** Public Methods ***/
		inst.onKeyPress = function(BlowFish, key) {
			if(key == instance.UP) {
				instance.resources.getSound(SOUND_FINGER).playSound();
				if(BlowFish.getRoom().id == "MainMenu") {
					if(inst.selection != 0) {
						inst.selection -= 1;
					}
					else {
						inst.selection = 2;
					}
				}
				else if(BlowFish.getRoom().id == "GamesMenu") {
						
				}
			}
			else if(key == instance.DOWN) {
				instance.resources.getSound(SOUND_FINGER).playSound();
				if(instance.BLOWFISH.getRoom().id == "MainMenu") {
					if(inst.selection != 2) {
						inst.selection += 1;
					}
					else {
						inst.selection = 0;
					}
				}
				else if(BlowFish.getRoom().id == "GamesMenu") {

				}
			}
			else if(key == instance.A) {
				if(BlowFish.getRoom().id == "MainMenu") {
					instance.resources.getSound(SOUND_FINGER).playSound();
					if(inst.selection == 0) {
						//TODO HERE
						//instance.ambientSound.stop();
						instance.startGame(instance.GAMES_ROOT + "Cephalopong/cephalopong.js");
						//BlowFish.setRoom(new GamesMenu());
					}
					else if(inst.selection == 1) {
						inst.x -= 25;
						inst.floatX -= 25;
						BlowFish.setRoom(new OptionsMenu());
					}
					else {
						BlowFish.setRoom(new AboutMenu());
					}
				}
				else if(BlowFish.getRoom().id == "GamesMenu") {

				}
			}
			else if(key == instance.B) {
				if(BlowFish.getRoom().id == "MainMenu") {

				}
				else if(BlowFish.getRoom().id == "GamesMenu") {
					instance.resources.getSound(SOUND_FINGER).playSound();
					BlowFish.setRoom(new MainMenu());
				}
				else if(BlowFish.getRoom().id == "OptionsMenu") {
					instance.resources.getSound(SOUND_FINGER).playSound();
					inst.x += 25;
					inst.floatX += 25;
					BlowFish.setRoom(new MainMenu());
				}
				else if(BlowFish.getRoom().id == "AboutMenu") {
					instance.resources.getSound(SOUND_FINGER).playSound();
					BlowFish.setRoom(new MainMenu());
				}
			}
		}

		inst.onUpdate = function(BlowFish) {
			if(inst.x < inst.floatX - 15 || inst.x > inst.floatX) {
				inst.xSpeed = -inst.xSpeed;
			}
			inst.x += inst.xSpeed;

			var s = inst.selection;
			if(s == 0) {
				inst.y = 165;
			}
			else if(s == 1) {
				inst.y = 215;
			}
			else if(s == 2) {
				inst.y = 265;
			}
		}

		return inst;
	}

	function Background() {
		/*** Private Variables ***/
		var inst = new BlowFish.GameObject(null, 0, 0, 0, 853, 480);

		/*** Public Variables ***/
		inst.class = "Background";
		inst.textColor = "#ffffff";
		inst.backgroundColor = "#777777";
		inst.redSpeed = 1;
		inst.greenSpeed = 2;
		inst.blueSpeed = 3;

		/*** Public Methods ***/
		inst.onUpdate = function(BlowFish) {
			inst.textColor = incrementColor(inst.textColor, inst);
			inst.backgroundColor = incrementColor(inst.backgroundColor, inst);
		}

		inst.onDraw = function(BlowFish) {
			var BlowFishG2D = BlowFish.getContext();
			BlowFishG2D.fillStyle = inst.backgroundColor;
			BlowFishG2D.fillRect(0, 0, BlowFish.getCanvasWidth(), BlowFish.getCanvasHeight());
			BlowFishG2D.fillStyle = inst.textColor;
			BlowFishG2D.font = 'bold 32pt "Comic Sans MS"';
			BlowFishG2D.fillText("BlowFish", 20, 50);
			BlowFishG2D.font = 'bold 16pt "Comic Sans MS"';
			BlowFishG2D.fillText("v1.0", BlowFish.getCanvasWidth() - 65, BlowFish.getCanvasHeight() - 20);
		}

		return inst;
	}
}