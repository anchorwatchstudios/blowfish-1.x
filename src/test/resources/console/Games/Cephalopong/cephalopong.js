/*** BlowFish ***/
function main(BlowFish) {
	var GAME_INSTANCE = this;
	var WIDTH = 853;
	var HEIGHT = 480;
	var GAME_SPEED = 1;
	var FPS = 60;
	var IMAGE_ROOT = "Games/Cephalopong/Images/";
	var AUDIO_ROOT = "Games/Cephalopong/Audio/";
	var GameObjects = new Array();
	var leftIsPressed = false;
	var rightIsPressed = false;
	var spaceIsPressed = false;
	var paused = true;
	//TODO SPRITES
	var R = new BlowFish.Resources();
	R.SPRITE_SQUID = R.addSprite(IMAGE_ROOT + "squid.png");
	R.SPRITE_BLOWFISH = R.addSprite(IMAGE_ROOT + "blowFish_64x64.png");
	R.SPRITE_BACKGROUND1 = R.addSprite(IMAGE_ROOT + "background_1.png");
	R.SPRITE_ANCHOR = R.addSprite(IMAGE_ROOT + "anchor.png");
	R.SPRITE_CHAIN = R.addSprite(IMAGE_ROOT + "chain.png");
	R.SPRITE_FISH1 = R.addSprite(IMAGE_ROOT + "fish_1.png");
	R.SPRITE_FISH2 = R.addSprite(IMAGE_ROOT + "fish_2.png");
	R.SOUND_BOING = R.addSound(AUDIO_ROOT + "boing.wav", false);
	R.getSound(R.SOUND_BOING).volume = 0.1;
	GAME_INSTANCE.R = R;


	//var squidSprite = new Sprite(IMAGE_ROOT + "squid.png");
	//var blowFishSprite = new Sprite(IMAGE_ROOT + "blowFish_64x64.png");
	//var background1Sprite = new Sprite(IMAGE_ROOT + "background_1.png");
	//var anchorSprite = new Sprite(IMAGE_ROOT + "anchor.png");
	//var chainSprite = new Sprite(IMAGE_ROOT + "chain.png");
	//var fish1Sprite = new Sprite(IMAGE_ROOT + "fish_1.png");
	//var fish2Sprite = new Sprite(IMAGE_ROOT + "fish_2.png");
	//var boingSound = new Sound(AUDIO_ROOT + "boing.wav", false);
	//boingSound.volume = 0.1;
	var player1 = new Player(1);

	BlowFish.setCanvasWidth(WIDTH);
	BlowFish.setCanvasHeight(HEIGHT);
	BlowFish.GAME_SPEED = GAME_SPEED;
	BlowFish.FPS = FPS;
	BlowFish.setRoom(new PrototypeLevel());

	/*** Levels ***/
	function PrototypeLevel() {
		var inst = new BlowFish.Room(WIDTH, HEIGHT);
		inst.add(new Background1());
		inst.add(new PrototypeLevelControl());
		inst.add(new Paddle(298, 415));
		inst.add(new Fish(400, 50, 0, .25));
		inst.add(new Fish(300, 225, 1, .5));
		inst.add(new Fish(110, 25, 1, .25));
		inst.add(new Fish(175, 90, 0, 1));
		inst.add(new Fish(500, 125, 0, .75));
		inst.add(new Fish(423, 200, 0, .5));
		inst.add(new Fish(356, 275, 0, .25));
		inst.add(new Ball(394, 350));
		inst.add(new Anchor(0, 350));
		inst.add(new Anchor(740, 350));
		inst.add(new Chain(45, -100));
		inst.add(new Chain(785, -100));
		return inst;
	}

	/*** Game Objects ***/
	function Paddle(x, y) {
		var instance = new BlowFish.GameObject(R.getSprite(R.SPRITE_SQUID), x, y, 0, 256, 64);
		instance.class = "Paddle";
		instance.solid = true;

		instance.onKeyPress = function(BlowFish, key) {
			if(paused == true) {
				paused = false;
			}
			else {
				if(key == BlowFish.BIOS.START) {
					paused = true;
				}
			}
		}

		instance.onKeyDown = function(BlowFish, key) {
			if(paused == false) {
				if(key == BlowFish.BIOS.LEFT) {
					instance.xSpeed = -5;
				}
				if(key == BlowFish.BIOS.RIGHT) {
					instance.xSpeed = 5;
				}
			}
		}

		instance.onKeyUp = function(BlowFish, key) {
			if(paused == false) {
				if(key == BlowFish.BIOS.LEFT) {
					instance.xSpeed = 0;
				}
				if(key == BlowFish.BIOS.RIGHT) {
					instance.xSpeed = 0;
				}
			}
		}

		instance.onCollision = function(BlowFish, object) {
			if(object.class == "Anchor") {
				//TODO HERE
				instance.xSpeed = 0;
			}
		}

		return instance;
	}

	function Ball(x, y) {
		var instance = new BlowFish.GameObject(R.getSprite(R.SPRITE_BLOWFISH), x, y, 0, 64, 64);
		var isNegative = ((Math.random()*10)+1) > 5;
		var speed = 5;
		if(isNegative) {
			speed = -speed;
		}
		instance.xSpeed = speed;
		instance.ySpeed = speed;
		instance.rSpeed = 0.1;
		instance.solid = true;
		instance.class = "Ball";

		instance.onUpdate = function(BlowFish) {
			if(paused == false) {
				if(instance.y <= 0) {
					GAME_INSTANCE.R.getSound(GAME_INSTANCE.R.SOUND_BOING).play();
					instance.ySpeed = -instance.ySpeed;
					if(instance.xSpeed > 0) {
						instance.rSpeed = -Math.abs(instance.rSpeed);
					}
					else {
						instance.rSpeed = Math.abs(instance.rSpeed);
					}
				}
				else if(instance.y >= BlowFish.getCanvasHeight() + instance.height) {
					paused = true;
					if(player1.lives > 0) {
						player1.lives -= 1;
						BlowFish.getRoom().GameObjects[2].x = 298;
						BlowFish.getRoom().GameObjects[2].y = 415;
						BlowFish.getRoom().GameObjects[BlowFish.getRoom().GameObjects.length-5].x = 394;
						BlowFish.getRoom().GameObjects[BlowFish.getRoom().GameObjects.length-5].y = 350;
					}
					else {
						alert("You lose!");
						BlowFish.BIOS.stopGame();
					}
				}
				instance.checkCollision(BlowFish);
				instance.x += instance.xSpeed;
				instance.y += instance.ySpeed;
				instance.r += instance.rSpeed;
			}
		}

		instance.onCollision = function(BlowFish, object) {
			if(object.class == "Paddle") {
				instance.bounce(object);
				if(instance.xSpeed > 0) {
					instance.xSpeed += 1;
				}
				else {
					instance.xSpeed -= 1;
				}
				instance.ySpeed -= 1;
			}
			if(object.class == "Anchor" || object.class == "Chain") {
				instance.bounce(object);
			}
			if(object.class == "Fish") {
				if(object.alive) {
					instance.bounce(object);
					if(object.health > 1) {
						object.health -= 1;
						var T = (object.y <= instance.y + instance.height) && (object.y > instance.y);
						var B = (object.y + object.height >= instance.y) && (object.y < instance.y);
						var L = (object.x <= instance.x + instance.width) && (object.x > instance.x);
						var R = (object.x + object.width >= instance.x) && (object.x < instance.x);
						if(T||B) {
							if(instance.x + instance.width > object.x) {
								object.xSpeed = -Math.abs(object.xSpeed);
							}
							else {
								object.xSpeed = Math.abs(object.xSpeed);
							}

							if(object.xSpeed > 0) {
								object.facingLeft = false;
							}
							else {
								object.facingLeft = true;
							}
						}
					}
					else {
						object.health = 0;
						object.xSpeed = 0;
						object.ySpeed = Math.floor(2*object.scale) + 1;
						object.alive = false;
					}
				}
			}
		}

		instance.bounce = function(object) {
			GAME_INSTANCE.R.getSound(GAME_INSTANCE.R.SOUND_BOING).playSound();
			var T = (object.y <= instance.y + instance.height) && (object.y > instance.y);
			var B = (object.y + object.height >= instance.y) && (object.y < instance.y);
			var L = (object.x <= instance.x + instance.width) && (object.x > instance.x);
			var R = (object.x + object.width >= instance.x) && (object.x < instance.x);
			if(T || B){
				instance.xSpeed = -instance.xSpeed;
				if(instance.ySpeed > 0) {
					if(object.x > instance.x) {
						instance.rSpeed = -Math.abs(instance.rSpeed);
					}
					else {
						instance.rSpeed = Math.abs(instance.rSpeed);
					}
				}
				else {
					if(object.x > instance.x) {
						instance.rSpeed = Math.abs(instance.rSpeed);
					}
					else {
						instance.rSpeed = -Math.abs(instance.rSpeed);
					}
				}
			}
			else if(L || R) {
				instance.ySpeed = -instance.ySpeed;
				if(instance.xSpeed > 0) {
					if(object.y > instance.y) {
						instance.rSpeed = Math.abs(instance.rSpeed);
					}
					else {
						instance.rSpeed = -Math.abs(instance.rSpeed);
					}
				}
				else {
					if(object.y > instance.y) {
						instance.rSpeed = -Math.abs(instance.rSpeed);
					}
					else {
						instance.rSpeed = Math.abs(instance.rSpeed);
					}
				}
			}
		}

		return instance;
	}

	function Fish(x, y, type, scale) {
		var instance;
		if(type == 0) {
			instance = new BlowFish.GameObject(R.getSprite(R.SPRITE_FISH1), x, y, 0, 192 * scale, 88 * scale);
		}
		else {
			instance = new BlowFish.GameObject(R.getSprite(R.SPRITE_FISH2), x, y, 0, 192 * scale, 88 * scale);
		}
		instance.class = "Fish";
		instance.scale = scale;
		instance.health = Math.floor(2*scale) + 1;
		instance.alive = true;
		var isNegative = ((Math.random()*10)+1) > 5;
		instance.facingLeft = isNegative;
		instance.xSpeed = ((Math.random()*10)+1);
		if(isNegative) {
			instance.xSpeed = -instance.xSpeed;
		}

		instance.onUpdate = function(BlowFish) {
			instance.checkCollision(BlowFish);
			instance.x += instance.xSpeed;
			instance.y += instance.ySpeed;
			instance.r += instance.rSpeed;

			if(instance.y >= BlowFish.getCanvasHeight() + instance.height) {
				BlowFish.getRoom().remove(instance);
			}
		}

		instance.onDraw = function(BlowFish) {
			var BlowFishG2D = BlowFish.getContext();
			var xScale = 0;
			var yScale = 0;
			if(instance.sprite && instance.visible) {
				if(instance.facingLeft && instance.alive) {
					xScale = 1;
					yScale = 1;
				}
				else if(instance.alive) {
					xScale = -1;
					yScale = 1;
				}
				else if(instance.facingLeft) {
					xScale = 1;
					yScale = -1;
				}
				else {
					xScale = -1;
					yScale = -1;
				}
				BlowFishG2D.save();
				BlowFishG2D.translate(instance.x, instance.y);
				BlowFishG2D.translate(instance.width/2, instance.height/2);
				BlowFishG2D.scale(xScale, yScale);
				BlowFishG2D.rotate(instance.r);
				BlowFishG2D.drawImage(instance.sprite, -(instance.width/2), -(instance.height/2), instance.width, instance.height);
				BlowFishG2D.restore();
			}
		}

		instance.onCollision = function(BlowFish, object) {
			if(object.class == "Anchor" || object.class == "Chain") {
				if(instance.alive) {
					instance.xSpeed = -instance.xSpeed;
					if(instance.xSpeed > 0) {
						instance.facingLeft = false;
					}
					else {
						instance.facingLeft = true;
					}
				}
			}
			if(object.class == "Paddle") {
				player1.score += Math.floor(instance.scale * 75);
				BlowFish.getRoom().remove(instance);
			}
		}

		return instance;
	}

	function Chain(x, y) {
		var instance = new BlowFish.GameObject(R.getSprite(R.SPRITE_CHAIN), x, y, 0, 25, 473);
		instance.class = "Chain";
		return instance;
	}

	function Anchor(x, y) {
		var instance = new BlowFish.GameObject(R.getSprite(R.SPRITE_ANCHOR), x, y, 0, 112, 128);
		instance.class = "Anchor";
		return instance;
	}

	function Background1() {
		var instance = new BlowFish.GameObject(R.getSprite(R.SPRITE_BACKGROUND1), 0, 0, 0, 853, 480);
		instance.class = "Background1";
		return instance;
	}

	function PrototypeLevelControl() {
		var instance = new BlowFish.GameObject(null, 0, 0, 0, 0, 0);
		instance.class = "Control";

		instance.onUpdate = function(BlowFish) {
			if(BlowFish.getRoom().GameObjects.length == 8) {
				alert("You Win!");
				BlowFish.BIOS.stopGame();
			}
		}

		instance.onDraw = function(BlowFish) {
			var BlowFishG2D = BlowFish.getContext();
			BlowFishG2D.fillStyle = "#ffffff";
			BlowFishG2D.font = 'bold 32pt "Comic Sans MS"';
			BlowFishG2D.fillText("Score: " + player1.score + "       Lives: " + player1.lives, 160, 40);
		}

		return instance;
	}

	function Player(number) {
		var instance = this;
		instance.number = number;
		instance.lives = 3;
		instance.score = 0;
	}
}