/* BlowFish Graphics Engine v1.0
 * 
 * Created by Anthony Atella,
 * � AnchorWatch Studios, 2014
 *
 * Legal Notice: The contents of this document are subject
 * to copyright infringement laws and may only be used for
 * educational and recreational use by any person(s) other 
 * than the author.
 */

/* public class BlowFish(canvasId, rootDir)
 * Description:
 * 	An HTML5 Graphics engine.
 * Params:
 * 	String canvasId - A valid HTML5 Canvas element id.
 * 	String rootDir - The root of the BlowFish directory.
 * Returns:
 * 	A new instance of "BlowFish".
 * Throws:
 * 	NullPointerException - If a canvas with the id "canvasId" cannot be found.
 *	NetworkError(404) - If "rootDir/BIOS/BIOS.js" cannot be found.
 */
function BlowFish(canvasId, rootDir) {
	/*** Private Variables ***/
	var MAX_FPS = 72;
	var ID = canvasId;
	var ROOT_DIR = rootDir + "/";
	var DISPLAY_CANVAS = new DisplayCanvas(canvasId, 4/3);
	var DRAWING_CANVAS = new DrawingCanvas();
	var GAME_LOOP = new GameLoop();
	var BLOWFISH_INSTANCE = this;
	var keyIsPressed = new Array();
	var targetFPS = 60;
	var targetGameSpeed = 1;
	var room = new Room(1, 1);

	/*** Main ***/
	init();

	/*** Public Methods ***/
	this.getId = function() {
		return ID;
	}

	this.getContext = function() {
		return DRAWING_CANVAS.getContext("2d");
	}

	this.getRootDir = function() {
		return ROOT_DIR;
	}

	this.getRoom = function() {
		return room;
	}

	this.setRoom = function(newRoom) {
		room = newRoom;
	}

	this.getFPS = function() {
		//TODO************************
		return targetFPS;
		//END TODO********************
	}

	this.getTargetFPS = function() {
		return targetFPS;
	}

	this.setTargetFPS = function(fps) {
		targetFPS = fps;
	}

	this.getGameSpeed = function() {
		//TODO************************
		return targetGameSpeed;
		//END TODO********************
	}

	this.getTargetGameSpeed = function() {
		return targetGameSpeed;
	}

	this.setTargetGameSpeed = function(gameSpeed) {
		targetGameSpeed = gameSpeed;
	}

	this.getCanvasWidth = function() {
		return DRAWING_CANVAS.width;
	}

	this.setCanvasWidth = function(width) {
		DRAWING_CANVAS.width = width;
	}

	this.getCanvasHeight = function() {
		return DRAWING_CANVAS.height;
	}

	this.setCanvasHeight = function(height) {
		DRAWING_CANVAS.height = height;
	}

	this.getDisplayWidth = function() {
		return DISPLAY_CANVAS.ELEMENT.width;
	}

	this.getDisplayHeight = function() {
		return DISPLAY_CANVAS.ELEMENT.height;
	}

	//TODO*************************************************************************************
	this.fullScreen = function() {
		var theCanvas = DISPLAY_CANVAS.ELEMENT.parentNode;
		if(theCanvas.requestFullScreen) {theCanvas.requestFullScreen();}
		else if(theCanvas.webkitRequestFullScreen) {theCanvas.webkitRequestFullScreen();}
		else if(theCanvas.mozRequestFullScreen) {theCanvas.mozRequestFullScreen();}
		else {alert("Error: Fullscreen is not supported.")}
	}

	this.restoreScreen = function() {
		var theCanvas = DISPLAY_CANVAS.ELEMENT.parentNode;
		if(theCanvas.cancelFullScreen) {theCanvas.cancelFullScreen();}
		else if(theCanvas.webkitCancelFullScreen) {theCanvas.webkitCancelFullScreen();}
		else if(theCanvas.mozCancelFullScreen) {theCanvas.mozCancelFullScreen();}
		else {alert("Error: Fullscreen is not supported.")}
	}

	this.isFullScreen = function() {
		return false;
	}
	//END TODO*********************************************************************************

	/*** Private Methods ***/
	function init() {
		//Add EventListeners
		$(window).resize(function() {
        		DISPLAY_CANVAS.resize(BLOWFISH_INSTANCE.getCanvasWidth()/BLOWFISH_INSTANCE.getCanvasHeight());
		});
		$(window).keydown(function(event) {
			var key = event.keyCode;
			room.onKeyDown(BLOWFISH_INSTANCE, key);
			var bool = true;
			for(var i=0;i<keyIsPressed;i+=1) {
				if(keyIsPressed[i] == key) {
					bool = false;
					break;
				}
			}
			if(bool) {
				keyIsPressed[keyIsPressed.length] = key;
				room.onKeyPress(BLOWFISH_INSTANCE, key);
			}
		});
		$(window).keyup(function(event) {
			var key = event.keyCode;
			room.onKeyUp(BLOWFISH_INSTANCE, key);
			for(var i=0;i<keyIsPressed;i+=1) {
				if(keyIsPressed[i] == key) {
					for(var j=i;j<keyIsPressed.length - 1;j+=1) {
						keyIsPressed[j] = keyIsPressed[j+1];
					}
					keyIsPressed.pop();
					break;
				}
			}
		});

		//Start the GameLoop
		GAME_LOOP.start();

		//Run the BIOS
		$.getScript("BIOS/BIOS.js", function() {
			main(BLOWFISH_INSTANCE);
			DISPLAY_CANVAS.resize(BLOWFISH_INSTANCE.getCanvasWidth()/BLOWFISH_INSTANCE.getCanvasHeight());
		}).fail(function() {
			window.alert('NetworkError(404): "' + 'BIOS/BIOS.js" could not be found');
		});
	}

	/*** Public Classes ***/
	this.Room = Room;
	this.GameObject = GameObject;
	this.Resources = Resources;

	/* public class Room(width, height)
	 * Description:
	 * 	A container to store GameObjects in.
	 * Params:
	 * 	int width - The width of the room in pixels (Original resolution).
	 * 	int height - The height of the room in pixels (Original resolution).
	 * Returns:
	 * 	A new instance of "Room".
	 */
	function Room(width, height) {
		/*** Public Variables ***/
		if(width) {
			this.width = width;
		}
		else {
			this.width = 640;
		}
		if(height) {
			this.height = height;
		}
		else {
			this.height = 480;
		}
		this.GameObjects = new Array();

		/*** Public Methods ***/
		this.onUpdate = function(BlowFish) {
			if(this.isLoaded()) {
				for(var i=0;i<this.GameObjects.length;i+=1) {
					this.GameObjects[i].onUpdate(BlowFish);
				}
			}
		}

		this.onDraw = function(BlowFish) {
			var BlowFishG2D = BlowFish.getContext();
			if(this.isLoaded()) {
				BlowFishG2D.fillStyle = "#000000";
				BlowFishG2D.fillRect(0, 0, BlowFish.getCanvasWidth(), BlowFish.getCanvasHeight());
				for(var i=0;i<this.GameObjects.length;i+=1) {
					this.GameObjects[i].onDraw(BlowFish);
				}
			}
		}

		this.onKeyPress = function(BlowFish, key) {
			for(var i=0;i<this.GameObjects.length;i+=1) {
				this.GameObjects[i].onKeyPress(BlowFish, key);
			}
		}

		this.onKeyDown = function(BlowFish, key) {
			for(var i=0;i<this.GameObjects.length;i+=1) {
				this.GameObjects[i].onKeyDown(BlowFish, key);
			}
		}

		this.onKeyUp = function(BlowFish, key) {
			for(var i=0;i<this.GameObjects.length;i+=1) {
				this.GameObjects[i].onKeyUp(BlowFish, key);
			}
		}

		this.add = function(gameObject) {
			gameObject.onCreate(BlowFish);
			this.GameObjects[this.GameObjects.length] = gameObject;
		}

		this.remove = function(gameObject) {
			gameObject.onDestroy();
			var index = -1;
			for(var i=0;i<this.GameObjects.length;i+=1) {
				if(this.GameObjects[i] === gameObject) {
					index = i;
					break;
				}
			}
			if(index > -1) {
				if(index != this.GameObjects.length-1) {
					for(var i=index+1;i<this.GameObjects.length;i+=1) {
						this.GameObjects[i-1] = this.GameObjects[i];
					}
				}
				this.GameObjects.pop();
			}
			else {
				alert("Error: Could not find the specified object in the room.");
			}
		}

		this.removeAll = function() {
			this.GameObjects = new Array();
		}

		this.isLoaded = function() {
			var bool = true;
			for(var i=0;i<this.GameObjects.length;i+=1) {
				if(!this.GameObjects[i].isLoaded()) {
					bool = false;
					break;
				}
			}
			return bool;
		}
	}

	/* public class GameObject(sprite, x, y, r, width, height)
	 * Description:
	 * 	A drawable, dynamic object.
	 * Params:
	 * 	Resources.Sprite sprite - The image resource(s) for this object.
	 * 	int x - X coordinate in pixels (Original Resolution).
	 * 	int y - Y coordinate in pixels (Original Resolution).
	 * 	int r - Rotation in radians.
	 * 	int width - Width of this object (Original Resolution).
	 * 	int height - Height of this object (Original Resolution).
	 * Returns:
	 * 	A new instance of "GameObject".
	 */
	function GameObject(sprite, x, y, r, width, height) {
		/*** Public Variables ***/
		this.class = "";
		this.visible = true;
		this.precise = false;
		this.sprite = sprite;
		this.xSpeed = 0;
		this.ySpeed = 0;
		this.rSpeed = 0;
		if(width) {
			this.width = width;
		}
		else if(sprite) {
			//TODO****************
			width = sprite.width;
			//END TODO************
		}
		else {
			width = 0;
		}
		if(height) {
			this.height = height;
		}
		else if(sprite) {
			//TODO****************
			height = sprite.height;
			//END TODO************
		}
		else {
			height = 0;
		}
		if(x) {
			this.x = x;
		}
		else {
			this.x = 0;
		}
		if(y) {
			this.y = y;
		}
		else {
			this.y = 0;
		}
		if(r) {
			this.r = r;
		}
		else {
			this.r = 0;
		}

		/*** Public Methods ***/
		this.onUpdate = function(BlowFish) {
			this.checkCollision(BlowFish);
			this.x += this.xSpeed;
			this.y += this.ySpeed;
			this.r += this.rSpeed;
		}

		this.onDraw = function(BlowFish) {
			var BlowFishG2D = BlowFish.getContext();
			if(this.sprite && this.visible) {
				BlowFishG2D.save();
				BlowFishG2D.translate(this.x, this.y);
				BlowFishG2D.translate(this.width/2, this.height/2);
				BlowFishG2D.rotate(this.r);
				BlowFishG2D.drawImage(this.sprite, -(this.width/2), -(this.height/2), this.width, this.height);
				BlowFishG2D.restore();
			}
		}

		this.onCreate = function(BlowFish) {

		}

		this.onDestroy = function(BlowFish) {

		}

		this.onCollision = function(BlowFish, object) {

		}

		this.onKeyPress = function(BlowFish, key) {

		}

		this.onKeyDown = function(BlowFish, key) {

		}

		this.onKeyUp = function(BlowFish, key) {

		}

		this.checkCollision = function(BlowFish) {
			var GameObjects = BlowFish.getRoom().GameObjects;
			for(var i=0;i<GameObjects.length;i+=1) {
				if(!(GameObjects[i] === this)) {

					var L = (this.x + this.xSpeed <= GameObjects[i].x + GameObjects[i].width + GameObjects[i].xSpeed) && (this.x + this.xSpeed > GameObjects[i].x + GameObjects[i].xSpeed);
					var R = (this.x + this.width + this.xSpeed >= GameObjects[i].x + GameObjects[i].xSpeed) && (this.x + this.xSpeed < GameObjects[i].x + GameObjects[i].xSpeed);
					var T = (this.y + this.ySpeed <= GameObjects[i].y + GameObjects[i].height + GameObjects[i].ySpeed) && (this.y + this.ySpeed > GameObjects[i].y + GameObjects[i].ySpeed);
					var B = (this.y + this.height + this.ySpeed >= GameObjects[i].y + GameObjects[i].ySpeed) && (this.y + this.ySpeed < GameObjects[i].y + GameObjects[i].ySpeed);
					if((L || R) && (T || B)) {
						if(this.precise) {
							//TODO***********************************************
								//Precise collision checking
							//END TODO*******************************************
						}
						else {
							this.onCollision(BlowFish, GameObjects[i]);
						}
					}
				}
			}
		}

		this.isLoaded = function() {
			if(sprite) {
				return this.sprite.loaded;
			}
			else {
				return true;
			}
		}
	}

	/* public class Resources()
	 * Description:
	 * 	A container that creates, loads, and returns 
	 * 	instances of the private sub-classes
	 * 	"Sprites" and "Sounds".
	 * Returns:
	 * 	A new instance of "Resources".
	 */
	function Resources() {
		this.Sprites = new Array();
		this.Sounds = new Array();
	
		this.addSprite = function(src) {
			this.Sprites[this.Sprites.length] = new Sprite(src);
			return this.Sprites.length - 1;
		}

		this.getSprite = function(index) {
			return this.Sprites[index];
		}

		this.addSound = function(src, loop) {
			if(!loop) {
				loop = false;
			}
			this.Sounds[this.Sounds.length] = new Sound(src, loop);
			return this.Sounds.length - 1;
		}

		this.getSound = function(index) {
			return this.Sounds[index];
		}

		this.purge = function() {
			this.Sprites = new Array();
			this.Sounds = new Array();
		}

		this.isLoaded = function() {
			var bool = true;
			for(var i=0;i<this.Sprites.length;i+=1) {
				if(!this.Sprites[i].isLoaded()) {
					bool = false;
					break;
				}
			}
			if(bool) {
				for(var j=0;i<this.Sounds.length;j+=1) {
					if(!this.Sounds[j].isLoaded()) {
						bool = false;
						break;
					}
				}
			}
			return bool;
		}

		/*** Private Classes ***/
		/* private class Sprite(src)
		 * Description:
		 * 	An array of images.
		 * Params:
		 * 	String[] src - An array of image source paths.
		 * Returns:
		 * 	A new instance of "Sprite".
		 * Throws:
		 * 	NetworkError(404) - If the image(s) cannot be found.
		 */
		//TODO*************************************************************************************
		//Sprite src array
		function Sprite(src) {
			var spriteInstance = new Image();
			spriteInstance.loaded = false;
			spriteInstance.isLoaded = isLoaded;
			$(spriteInstance).load(function() {
				spriteInstance.loaded = true;
			}).error(function() {
				window.alert('NetworkError(404): "' + src + '" could not be found.');
			}).attr("src", src);

			function isLoaded() {
				return spriteInstance.loaded;
			}

			return spriteInstance;
		}
		//END TODO*********************************************************************************

		/* private class Sound(src, loop)
		 * Description:
		 * 	A sound resource.
		 * Params:
		 * 	String src - The source path to the sound.
		 * 	boolean loop - If set to true, the sound will loop.
		 * Returns:
		 * 	A new instance of "Sound".
		 * Throws:
		 * 	NetworkError(404) - If the sound cannot be found.
		 * 	TypeError - If the sound fails to load.
		 */
		function Sound(src, loop) {
			var soundInstance = new Audio(src);
			soundInstance.loaded = false;
			soundInstance.preload = "auto";
			soundInstance.volume = 1;
			$(soundInstance).load(function() {
				soundInstance.loaded = true;
			}).error(function(event) {
				var code = event.delegateTarget.error.code;
				if(code == 4) {
					window.alert('NetworkError(404): "' + src + '" could not be found.');
				}
				else {
					window.alert('TypeError: "' + src + '" failed to load.');
				}
			});
			if(loop == true) {
				soundInstance.loop = true;
			}

			soundInstance.playSound = function() {
				var clone = soundInstance.cloneNode(true);
				clone.volume = soundInstance.volume;
				clone.play();
			}

			soundInstance.stopSound = function() {
				//TODO*******************************
				//END TODO***************************
			}

			return soundInstance;
		}
	}

	/*** Private Classes ***/
	/* private class DrawingCanvas()
	 * Description:
	 * 	A hidden canvas used for pre-rendered drawing.
	 * Returns:
	 * 	A new instance of "DrawingCanvas".
	 */
	function DrawingCanvas() {
		var element = document.createElement("canvas");
		element.className  = "DrawingCanvas";
		element.id = "drawingCanvas";
		element.width = 1;
		element.height = 1;
		return element;
	}

	/* private class DisplayCanvas(id, aspectRatio)
	 * Description:
	 * 	A canvas used for rendering.
	 * Params:
	 * 	String id - A valid HTML5 Canvas element id.
	 * 	float aspectRatio - The aspect ratio of the canvas.
	 * Returns:
	 * 	A new instance of "DisplayCanvas".
	 * Throws:
	 * 	NullPointerException - If a canvas with the id "id" cannot be found.
	 */
	function DisplayCanvas(id, aspectRatio) {
		this.resize = resize;
		this.ELEMENT = document.getElementById(id);
		if(!this.ELEMENT) {
			window.alert('NullPointerException: document.getElementById("' + id + '") returned null.');
		}
		this.ELEMENT.style.display = "block";
		this.ELEMENT.style.margin = "0 auto";
		this.BlowFishG2D = this.ELEMENT.getContext("2d");
		if(aspectRatio) {
			this.resize(aspectRatio);
		}
		else {
			this.resize(4/3);
		}

		function resize(aspectRatio) {
			var w = this.ELEMENT.parentNode.offsetWidth;
			var h = this.ELEMENT.parentNode.offsetHeight;
			var finalWidth = 640;
			var finalHeight = 480;
			if(w > h*aspectRatio) {
				finalHeight = h;
				finalWidth = Math.floor(h*aspectRatio);
			}
			else {
				finalWidth = w;
				finalHeight = Math.floor(w/aspectRatio);
			}
			this.ELEMENT.width = finalWidth;
			this.ELEMENT.height = finalHeight;
			this.ELEMENT.style.padding = Math.floor((h - this.ELEMENT.height)/2) + "px 0px 0px 0px ";
		}
	}

	/* private class GameLoop()
	 * Description:
	 * 	A timing thread used to sync-up drawing and updating.
	 * Returns:
	 * 	A new instance of "GameLoop".
	 */
	function GameLoop() {
		/*** Private Variables ***/
		var currentTime = parseInt(new Date().getTime());
		var lastUpdate = parseInt(new Date().getTime());
		var lastDraw = parseInt(new Date().getTime());

		/*** Public Methods ***/
		this.start = function() {
			this.Loop = setInterval(function() {
				//TODO*********************************************************************************************************************************
				//Optimize
				currentTime = parseInt(new Date().getTime());
				//Update
				if((currentTime - lastUpdate) >= 1000/(targetGameSpeed*30)) {
					room.onUpdate(BLOWFISH_INSTANCE);
					lastUpdate = parseInt(new Date().getTime());
				}
				//Draw
				if((currentTime - lastDraw) >= (1000/targetFPS)) {
					room.onDraw(BLOWFISH_INSTANCE);

					//Render
					var BlowFishG2D = DISPLAY_CANVAS.ELEMENT.getContext("2d");
					var BlowFishG2D_Draw = DRAWING_CANVAS.getContext("2d");
					BlowFishG2D.drawImage(BlowFishG2D_Draw.canvas, 0, 0, BLOWFISH_INSTANCE.getDisplayWidth(), BLOWFISH_INSTANCE.getDisplayHeight());
					
					lastDraw = parseInt(new Date().getTime());
				}
				//END TODO*****************************************************************************************************************************
			}, 1000/MAX_FPS);
		}

		this.stop = function() {
			clearInterval(this.Loop);
		}
	}
}