/**Package Summary*****************************************************
 *                              BlowFish,
 *                       an HTML5 graphics API.
 *                        Level 1.0: SugarToad
 * 
 *                ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 
 * Name
 * 	anchorWatchStudios.BlowFish.SugarToad
 * 
 * Contents
 * 	- SugarToad
 * 		- blowfish.js
 * 		- Documentation
 * 			- sugartoad.html
 * 			- sugartoad.rtf
 * 			- sugartoad.txt
 * 			- blowfish.ink
 * 			- HelloBlowFish
 * 				- helloblowfish.html
 * 				- Images
 * 					- blowfish.ico
 * 
 * Description
 * 		BlowFish is an HTML5 graphics API written in JavaScript. In 
 * 	the BlowFish graphics model, there are six basic classes 
 * 	available for use; BlowFishResources, BlowFishObject2D, 
 * 	BlowFishObject3D, BlowFishRoom2D, BlowFishRoom3D, 
 * 	BlowFishActivity, and BlowFish. As of this level, 
 * 	BlowFishObject3D and BlowFishRoom3D are unimplemented due to the 
 * 	experimental nature of HTML5 3D.
 * 		The BlowFish graphics model is as follows:
 *  ________________________________________________________________
 * |BlowFish                                                        |
 * |     ______________________________________________________     |
 * |    |BlowFishActivity______________________                |    |
 * |    |          |\                         |\               |    |
 * |    |          | \ _______________________|_\              |    |
 * |    |          |  |BlowFishRoom3D_______  |  |             |    |
 * |    |          |  | |BlowFishRoom2D     | |  |             |    |
 * |    |          |  | | |Camera|          | |  |             |    |
 * |    |          |__|_| |______|          |_|  |             |    |
 * |    |          \  | |___________________| \  |             |    |
 * |    |           \ |                        \ |             |    |
 * |    |            \|_________________________\|             |    |
 * |    |______________________________________________________|    |
 * |                                                                |
 * |________________________________________________________________|
 * 
 * 	The BlowFishRoom2D's position is relative to the Camera, and the 
 * Camera moves inside of the BlowFishRoom3D. BlowFishObject2Ds and 
 * BlowFishObject3Ds can be added to BlowFishRoom2Ds and 
 * BlowFishRoom3Ds, respectively. BlowFishResources automatically load 
 * audio-visual resources and said resources are linked to objects in 
 * the room. A BlowFishActivity is an application that runs on an 
 * instance of BlowFish and facilitates the manipulation of the 
 * BlowFish and it's sub-components. Finally, a BlowFish encapsulates 
 * the BlowFishActivity, linking it to a Canvas element on the HTML 
 * page. The camera takes a picture "through" the 2D plane and into the 
 * 3D perspective, and the image is resized and rendered onto the 
 * supplied canvas.
 * 		A "net" (parent HTML element) is required around the 
 * 	BlowFish to control its size. The BlowFish automatically "puffs 
 * 	up" to the size of its parent element.
 * Documentation
 * 		API documentation and tutorials can be found at 
 * 	"http://www.AnchorWatchStudios.com/blowfish.html".
 * Legal Notice
 * 		The contents of this document are subject to copyright 
 * 	infringement laws and may only be used for educational and 
 * 	recreational purposes by any person(s) other than the author.
 * 
 *                ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 
 *                Created and written by Anthony Atella,
 *                     � AnchorWatch Studios, 2014
 *********************************************************************/

/*** Package Classes ***/
/**public class BlowFish(canvasId, blowFishActivity)*******************
 * 	Description
 * 		An HTML5 Graphics engine.
 * 	Parameters
 * 		String canvasId - A valid HTML5 Canvas element id.
 * 		BlowFishActivity blowFishActivity - A program to run after 
 * 			initialization.
 * 	Returns
 * 		A new instance of BlowFish.
 * 	Throws
 * 		NullPointerException - If a canvas with the given id
 * 			cannot be found, or blowFishActivity.main is null.
 * 		CompatibilityException - If the target API level of 
 * 			blowFishActivity exceeds the API level of this 
 * 			instance.
 * 		UncaughtException - If there is an error in 
 * 			blowFishActivity.main.
 *********************************************************************/
function BlowFish(canvasId, blowFishActivity) {

	/*** Private Variables ***/
	/**private static final double API_LEVEL*************************
	 * Description
	 * 	The API level of this instance. Used for backward 
	 * 	compatibility.
	 ***************************************************************/
	var API_LEVEL = 1.0;

	/**private static final BlowFish BLOWFISH_INSTANCE***************
	 * Description
	 * 	A handle to this instance.
	 ***************************************************************/
	var BLOWFISH_INSTANCE = this;

	/**private static final int MAX_FPS******************************
	 * Description
	 * 	Maximum frames per second. A hard-coded delimeter set by 
	 * 	the author to prevent potential damage to the user's 
	 * 	machine.
	 ***************************************************************/
	var MAX_FPS = 72;

	/**private static final int MAX_UPS******************************
	 * Description
	 * 	Maximum updates per second. A hard-coded delimeter set by 
	 * 	the author to prevent potential damage to the user's 
	 * 	machine.
	 ***************************************************************/
	var MAX_UPS = 72;

	/**private static final String ID********************************
	 * Description
	 * 	The HTML Element id of this instance.
	 ***************************************************************/
	var ID = canvasId;

	/**private static final DisplayCanvas DISPLAY_CANVAS*************
	 * Description
	 * 	The DisplayCanvas of this instance.
	 ***************************************************************/
	var DISPLAY_CANVAS = new DisplayCanvas(canvasId);

	/**private static final DrawingCanvas DRAWING_CANVAS*************
	 * Description
	 * 	The DrawingCanvas of this instance.
	 ***************************************************************/
	var DRAWING_CANVAS = new DrawingCanvas();

	/**private static final TimingThread TIMING_THREAD***************
	 * Description
	 * 	The TimingThread of this instance.
	 ***************************************************************/
	var TIMING_THREAD = new TimingThread();

	/**private int[] keyIsPressed************************************
	 * Description
	 * 	An array of key-codes; the keys currently pressed.
	 ***************************************************************/
	var keyIsPressed = new Array();

	/**private Object[] mouseIsHovering******************************
	 * Description
	 * 	An array of objects; the objects currently being 
	 * 	hovered over.
	 ***************************************************************/
	var mouseIsHovering = new Array();

	/**private int targetFPS*****************************************
	 * Description
	 * 	The current target frames per second.
	 ***************************************************************/
	var targetFPS = 60;

	/**private int targetUPS*****************************************
	 * Description
	 * 	The current target updates per second.
	 ***************************************************************/
	var targetUPS = 60;

	/**private BlowFishRoom2D room2D*********************************
	 * Description
	 * 	The current 2D room.
	 ***************************************************************/
	var room2D = new BlowFishRoom2D();

	/**private BlowFishRoom3D room3D*********************************
	 * Description
	 * 	The current 3D room.
	 ***************************************************************/
	var room3D = new BlowFishRoom3D();

	/*** Public Methods ***/
	/**public double getAPILevel()***********************************
	 * Description
	 * 	Returns the API level of this instance.
	 * Returns
	 * 	API_LEVEL
	 ***************************************************************/
	this.getAPILevel = function() {
		return API_LEVEL;
	}

	/**public String getId()*****************************************
	 * Description
	 * 	Returns the HTML Element id of this instance.
	 * Returns
	 * 	ID
	 ***************************************************************/
	this.getId = function() {
		return ID;
	}

	/**public Room get2DRoom()***************************************
	 * Description
	 * 	Returns the current 2D room.
	 * Returns
	 * 	room2D
	 ***************************************************************/
	this.get2DRoom = function() {
		return room2D;
	}

	/**public void set2DRoom(newRoom)********************************
	 * Description
	 * 	Sets the current 2D room to the supplied room.
	 * Parameters
	 * 	BlowFishRoom2D newRoom - A new 2D Room.
	 ***************************************************************/
	this.set2DRoom = function(newRoom) {
		room2D = newRoom;
	}

	/**public Room get3DRoom()***************************************
	 * Description
	 * 	Returns the current 3D room.
	 * Returns
	 * 	room3D
	 ***************************************************************/
	this.get3DRoom = function() {
		return room3D;
	}

	/**public void set3DRoom(newRoom)********************************
	 * Description
	 * 	Sets the current 3D room to the supplied room.
	 * Parameters
	 * 	BlowFishRoom3D newRoom - A new 3D Room.
	 ***************************************************************/
	this.set3DRoom = function(newRoom) {
		room3D = newRoom;
	}

	/**public int getFPS()*******************************************
	 * Description
	 * 	Returns the current frames per second.
	 * Returns
	 * 	TIMING_THREAD.getFPS()
	 ***************************************************************/
	this.getFPS = function() {
		return TIMING_THREAD.getFPS();
	}

	/**public int getTargetFPS()*************************************
	 * Description
	 * 	Returns the current target frames per second.
	 * Returns
	 * 	targetFPS
	 ***************************************************************/
	this.getTargetFPS = function() {
		return targetFPS;
	}

	/**public void setTargetFPS(fps)*********************************
	 * Description
	 * 	Sets the target frames per second.
	 * Parameters
	 * 	int fps - The desired target frames per second.
	 ***************************************************************/
	this.setTargetFPS = function(fps) {
		targetFPS = fps;
	}

	/**public int getUPS()*******************************************
	 * Description
	 * 	Returns the current updates per second.
	 * Returns
	 * 	TIMING_THREAD.getUPS()
	 ***************************************************************/
	this.getUPS = function() {
		return TIMING_THREAD.getUPS();
	}

	/**public int getTargetUPS()*************************************
	 * Description
	 * 	Returns the current target updates per second.
	 * Returns
	 * 	targetUPS
	 ***************************************************************/
	this.getTargetUPS = function() {
		return targetUPS;
	}

	/**public void setTargetUPS(ups)*********************************
	 * Description
	 * 	Sets the target updates per second.
	 * Parameters
	 * 	int ups - The desired target updates per second.
	 ***************************************************************/
	this.setTargetUPS = function(ups) {
		targetUPS = ups;
	}

	/**public int getCanvasWidth()***********************************
	 * Description
	 * 	Returns the width of the drawing canvas in pixels.
	 * Returns
	 * 	 DRAWING_CANVAS.width
	 ***************************************************************/
	this.getCanvasWidth = function() {
		return DRAWING_CANVAS.width;
	}

	/**public void setCanvasWidth(width)*****************************
	 * Description
	 * 	Sets the width of the drawing canvas.
	 * Parameters
	 * 	 width - The desired width of the drawing canvas in pixels.
	 ***************************************************************/
	this.setCanvasWidth = function(width) {
		DRAWING_CANVAS.width = width;
		DISPLAY_CANVAS.resize(BLOWFISH_INSTANCE.getCanvasWidth() 
			/ BLOWFISH_INSTANCE.getCanvasHeight());
	}

	/**public int getCanvasHeight()**********************************
	 * Description
	 * 	Returns the height of the drawing canvas in pixels.
	 * Returns
	 * 	 DRAWING_CANVAS.height
	 ***************************************************************/
	this.getCanvasHeight = function() {
		return DRAWING_CANVAS.height;
	}

	/**public void setCanvasHeight(height)***************************
	 * Description
	 * 	Sets the height of the drawing canvas.
	 * Parameters
	 * 	height - The desired height of the drawing canvas in 
	 * 		pixels.
	 ***************************************************************/
	this.setCanvasHeight = function(height) {
		DRAWING_CANVAS.height = height;
		DISPLAY_CANVAS.resize(BLOWFISH_INSTANCE.getCanvasWidth() 
			/ BLOWFISH_INSTANCE.getCanvasHeight());
	}

	/**public int getDisplayWidth()*********************************
	 * Description
	 * 	Returns the width of the display canvas in pixels.
	 * Returns
	 * 	 DISPLAY_CANVAS.width
	 ***************************************************************/
	this.getDisplayWidth = function() {
		return DISPLAY_CANVAS.width;
	}

	/**public int getDisplayHeight()*********************************
	 * Description
	 * 	Returns the height of the display canvas in pixels.
	 * Returns
	 * 	 DISPLAY_CANVAS.height
	 ***************************************************************/
	this.getDisplayHeight = function() {
		return DISPLAY_CANVAS.height;
	}

	/**public void fullScreen()**************************************
	 * Description
	 * 	Sends a request to the browser to enter fullscreen mode.
	 * Throws
	 * 	UnsupportedException - If fullscreen is not supported by 
	 * 		the browser.
	 ***************************************************************/
	this.fullScreen = function() {
		var theCanvas = DISPLAY_CANVAS.parentNode;
		if(theCanvas.requestFullscreen) {
			theCanvas.requestFullscreen();
		} 
		else if(theCanvas.msRequestFullscreen) {
			theCanvas.msRequestFullscreen();
		} 
		else if(theCanvas.mozRequestFullScreen) {
			theCanvas.mozRequestFullScreen();
		} 
		else if(theCanvas.webkitRequestFullScreen) {
			theCanvas.webkitRequestFullScreen();
		}
		else {
			throwError("UnsupportedException",
				"Your browser does not support fullscreen");
		}
	}

	/**public void restoreScreen()***********************************
	 * Description
	 * 	Sends a request to the browser to exit fullscreen mode.
	 * Throws
	 * 	UnsupportedException - If fullscreen is not supported by 	 * 		the browser.
	 ***************************************************************/
	this.restoreScreen = function() {
		if(document.exitFullscreen) {
			document.exitFullscreen();
		} 
		else if(document.msExitFullscreen) { 
			document.msExitFullscreen();
		} 
		else if(document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} 
		else if(document.webkitExitFullScreen) {
			document.webkitExitFullScreen();
		} 
		else if(document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		}
		else {
			throwError("UnsupportedException",
				"Your browser does not support fullscreen");
		}
	}

	/**public boolean isFullScreen()*********************************
	 * Description
	 * 	Returns true if the browser is in fullscreen mode.
	 * Returns
	 * 	True if the browser is in fullscreen mode.
	 * Throws
	 * 	UnsupportedException - If fullscreen is not supported by 	 * 		the browser.
	 ***************************************************************/
	this.isFullScreen = function() {
		if(!document.exitFullscreen
			&& !document.msExitFullscreen
			&& !document.mozCancelFullScreen
			&& !document.webkitExitFullScreen
			&& !document.webkitCancelFullScreen) {
			throwError("UnsupportedException",
				"Your browser does not support fullscreen");
		}
		return !(!document.fullscreenElement 
			&& !document.mozFullScreenElement 
			&& !document.webkitFullscreenElement 
			&& !document.webkitCurrentFullScreenElement 
			&& !document.msFullscreenElement);
	}

	/**public void onError(event)************************************
	 * Description
	 * 	The error event for this instance.
	 * Parameters
	 * 	Object event - An event object, containing an error code 
	 * 		and a message. The error code can be accessed through 
	 * 		event.code and the error message can be accessed 
	 * 		through event.message.
	 ***************************************************************/
	this.onError = function(event) {
		window.alert(event.type + ": " + event.message);
	}

	/*** Private Methods ***/
	/**private void init()*******************************************
	 * Description
	 * 	Initializes the instance at construction. During this 
	 * 	process event listeners are added, the timing thread is 
	 * 	started, and the activity is started.
	 ***************************************************************/
	function init() {
		addEventListeners();
		if(blowFishActivity.getTargetAPILevel() 
			<= BLOWFISH_INSTANCE.getAPILevel()) {
			blowFishActivity.main(BLOWFISH_INSTANCE);
			TIMING_THREAD.start();
		}
		else {
			throwError("CompatibilityException",
				"The API level of the activity supplied"
				+ " exceeds the API level of the"
				+ " version of BlowFish supplied.");
		}
	}

	/**private void throwError(type, message)************************
	 * Description
	 * 	Throws an error with the given type and message.
	 * Parameters
	 * 	String type - An error type.
	 * 	String message - An error message.
	 ***************************************************************/
	function throwError(type, message) {
		var o = new Object();
		o.type = type;
		o.message = message;
		BLOWFISH_INSTANCE.onError(o);
	}
	/**private void addEventListeners()******************************
	 * Description
	 * 	Adds event listeners to the document.
	 ***************************************************************/
	function addEventListeners() {
		window.onresize = function() {
        		DISPLAY_CANVAS.resize(
				BLOWFISH_INSTANCE.getCanvasWidth()
				/ BLOWFISH_INSTANCE.getCanvasHeight());
		}
		DISPLAY_CANVAS.onmousedown = function(event) {
			var xpos = event.clientX 
				- DISPLAY_CANVAS.offsetLeft;
			var ypos = event.clientY 
				- DISPLAY_CANVAS.offsetTop;
			var ratio = BLOWFISH_INSTANCE.getCanvasWidth() 
				/ BLOWFISH_INSTANCE.getDisplayWidth();
			var x = Math.floor(xpos*ratio);
			var y = Math.floor(ypos*ratio);

			for(var i=0;i<room2D.objects.length;i+=1) {
				if((x >= room2D.objects[i].x 
						&& x < room2D.objects[i].x 
						+ room2D.objects[i].width)
					&& (y >= room2D.objects[i].y 
						&& y < room2D.objects[i].y 
						+ room2D.objects[i].height)) {
					room2D.objects[i].onMouseDown(x, y, 0);
				}
			}
		}
		DISPLAY_CANVAS.onmouseup = function(event) {
			var xpos = event.clientX 
				- DISPLAY_CANVAS.offsetLeft;
			var ypos = event.clientY 
				- DISPLAY_CANVAS.offsetTop;
			var ratio = BLOWFISH_INSTANCE.getCanvasWidth() 
				/ BLOWFISH_INSTANCE.getDisplayWidth();
			var x = Math.floor(xpos*ratio);
			var y = Math.floor(ypos*ratio);

			for(var i=0;i<room2D.objects.length;i+=1) {
				if((x >= room2D.objects[i].x 
						&& x < room2D.objects[i].x 
						+ room2D.objects[i].width)
					&& (y >= room2D.objects[i].y 
						&& y < room2D.objects[i].y 
						+ room2D.objects[i].height)) {
					room2D.objects[i].onMouseUp(x, y, 0);
				}
			}
		}

		DISPLAY_CANVAS.onclick = function(event) {
			var xpos = event.clientX 
				- DISPLAY_CANVAS.offsetLeft;
			var ypos = event.clientY 
				- DISPLAY_CANVAS.offsetTop;
			var ratio = BLOWFISH_INSTANCE.getCanvasWidth() 
				/ BLOWFISH_INSTANCE.getDisplayWidth();
			var x = Math.floor(xpos*ratio);
			var y = Math.floor(ypos*ratio);

			for(var i=0;i<room2D.objects.length;i+=1) {
				if((x >= room2D.objects[i].x 
						&& x < room2D.objects[i].x 
						+ room2D.objects[i].width)
					&& (y >= room2D.objects[i].y 
						&& y < room2D.objects[i].y 
						+ room2D.objects[i].height)) {
					room2D.objects[i].onMouseClick(x, y, 
						0);
				}
			}
		}

		DISPLAY_CANVAS.ondblclick = function(event) {
			var xpos = event.clientX 
				- DISPLAY_CANVAS.offsetLeft;
			var ypos = event.clientY 
				- DISPLAY_CANVAS.offsetTop;
			var ratio = BLOWFISH_INSTANCE.getCanvasWidth() 
				/ BLOWFISH_INSTANCE.getDisplayWidth();
			var x = Math.floor(xpos*ratio);
			var y = Math.floor(ypos*ratio);

			for(var i=0;i<room2D.objects.length;i+=1) {
				if((x >= room2D.objects[i].x 
						&& x < room2D.objects[i].x 
						+ room2D.objects[i].width)
					&& (y >= room2D.objects[i].y 
						&& y < room2D.objects[i].y 
						+ room2D.objects[i].height)) {
					room2D.objects[i].onMouseDoubleClick(x, 
						y);
				}
			}
		}

		DISPLAY_CANVAS.oncontextmenu = function(event) {
			var xpos = event.clientX 
				- DISPLAY_CANVAS.offsetLeft;
			var ypos = event.clientY 
				- DISPLAY_CANVAS.offsetTop;
			var ratio = BLOWFISH_INSTANCE.getCanvasWidth() 
				/ BLOWFISH_INSTANCE.getDisplayWidth();
			var x = Math.floor(xpos*ratio);
			var y = Math.floor(ypos*ratio);

			for(var i=0;i<room2D.objects.length;i+=1) {
				if((x >= room2D.objects[i].x 
						&& x < room2D.objects[i].x 
						+ room2D.objects[i].width)
					&& (y >= room2D.objects[i].y 
						&& y < room2D.objects[i].y 
						+ room2D.objects[i].height)) {
					room2D.objects[i].onMouseClick(x, y, 
						2);
				}
			}
		}

		DISPLAY_CANVAS.onmousemove = function(event) {
			var xpos = event.clientX 
				- DISPLAY_CANVAS.offsetLeft;
			var ypos = event.clientY 
				- DISPLAY_CANVAS.offsetTop;
			var ratio = BLOWFISH_INSTANCE.getCanvasWidth() 
				/ BLOWFISH_INSTANCE.getDisplayWidth();
			var x = Math.floor(xpos*ratio);
			var y = Math.floor(ypos*ratio);

			for(var i=0;i<room2D.objects.length;i+=1) {
				var ishovering = false;
				var index = -1;
				for(var j=0;j<mouseIsHovering.length;j+=1) {
					if(mouseIsHovering[j] 
						=== room2D.objects[i]) {
						ishovering = true;
						index = j;
						break;
					}
				}
				if((x >= room2D.objects[i].x 
						&& x < room2D.objects[i].x 
						+ room2D.objects[i].width)
					&& (y >= room2D.objects[i].y 
						&& y < room2D.objects[i].y 
						+ room2D.objects[i].height)) {
					if(!ishovering) {
						mouseIsHovering.push(
							room2D.objects[i]);
						room2D.objects[i].onMouseOver(x, 
							y);
					}
					room2D.objects[i].onMouseHover(x, y);
				}
				else {
					if(ishovering) {
						if(index == 0) {
							mouseIsHovering.shift();
						}
						else if(index 
							== mouseIsHovering.length 
								- 1) {
							mouseIsHovering.pop();
						}
						else {
							for(var k = index;k < 
								mouseIsHovering.length 
									- 1; k+=1) {
								mouseIsHovering[k] 
									= mouseIsHovering										[k+1];
							}
							mouseIsHovering.pop();
						}
						room2D.objects[i].onMouseOut(x, 
							y);
					}
				}
			}
		}

		window.onkeydown = function(event) {
			var key = event.keyCode;
			for(var i=0;i<room2D.objects.length;i+=1) {
				room2D.objects[i].onKeyDown(key);
			}
			var bool = true;
			for(var i=0;i<keyIsPressed;i+=1) {
				if(keyIsPressed[i] == key) {
					bool = false;
					break;
				}
			}
			if(bool) {
				keyIsPressed[keyIsPressed.length] = key;
				for(var i=0;i<room2D.objects.length;i+=1) {
					room2D.objects[i].onKeyPress(key);
				}
			}
		}
		window.onkeyup = function(event) {
			var key = event.keyCode;
			for(var i=0;i<room2D.objects.length;i+=1) {
				room2D.objects[i].onKeyUp(key);
			}
			for(var i=0;i<keyIsPressed;i+=1) {
				if(keyIsPressed[i] == key) {
					for(var j=i;j<keyIsPressed.length - 1;
						j+=1) {
						keyIsPressed[j] = 
							keyIsPressed[j+1];
					}
					keyIsPressed.pop();
					break;
				}
			}
		}
	}

	/*** Private Classes ***/
	/**private class DrawingCanvas() extends Canvas******************
	 * Description
	 * 	A hidden canvas used for drawing.
	 * Returns
	 * 	A new instance of DrawingCanvas.
	 ***************************************************************/
	function DrawingCanvas() {

		/*** Private Variables ***/
		/**private static final Canvas DRAWING_CANVAS_INSTANCE***** 
		 * Description
		 * 	A handle to the prototype for this instance.
		 *********************************************************/
		var DRAWING_CANVAS_INSTANCE = 
			document.createElement("canvas");

		/*** Public Methods ***/
		/**public BlowFishG2D get2DContext()*********************** 
		 * Description
		 * 	Returns the 2D drawing context for this instance.
		 * Returns
		 * 	new BlowFishG2D.
		 *********************************************************/
		DRAWING_CANVAS_INSTANCE.get2DContext = function() {
			return new BlowFishG2D();
		};

		/*** Private Methods ***/
		/**private void init()*************************************
		 * Description
		 * 	Called at construction. During this time, the width 
		 * 	and height are set to default values.
		 *********************************************************/
		function init() {
			DRAWING_CANVAS_INSTANCE.width = 640;
			DRAWING_CANVAS_INSTANCE.height = 480;
		}

		/*** Private Classes ***/
		/**private BlowFishG2D extends Canvas.Context2d************ 
		 * Description
		 * 	The 2D drawing context for this instance.
		 *********************************************************/
		function BlowFishG2D() {

			/*** Private Variables ***/
			/**private static final Canvas.Context2d*************
			 *****BLOWFISHG2D_INSTANCE***************************
		 	 * Description
		 	 * 	A handle to the prototype for this instance.
		 	 ***************************************************/
			var BLOWFISHG2D_INSTANCE 
				= DRAWING_CANVAS_INSTANCE.getContext("2d");

			/*** Public Methods ***/
			/**public BlowFishRoom2D getRoom*********************
		 	 * Description
		 	 * 	Returns the current 2D room for this instance.
		 	 * Returns
		 	 * 	BLOWFISH_INSTANCE.get2DRoom()
		 	 ***************************************************/
			BLOWFISHG2D_INSTANCE.getRoom = function() {
				return BLOWFISH_INSTANCE.get2DRoom();
			};

			return BLOWFISHG2D_INSTANCE;
		}

		init();

		return DRAWING_CANVAS_INSTANCE;
	}

	/**private class DisplayCanvas(id) extends Canvas****************
	 * Description
	 * 	A canvas used for rendering.
	 * Parameters
	 * 	String id - A valid HTML5 Canvas element id.
	 * Returns
	 * 	A new instance of DisplayCanvas.
	 * Throws
	 * 	NullPointerException - If a canvas with the given id cannot 
	 * 		be found.
	 ***************************************************************/
	function DisplayCanvas(id) {

		/*** Private Variables ***/
		/**private static final Canvas DISPLAY_CANVAS_INSTANCE*****
		 * Description
		 * 	A handle to the prototype for this instance.
		 *********************************************************/
		var DISPLAY_CANVAS_INSTANCE = document.getElementById(id);
		
		/*** Public Methods ***/
		/**public Canvas.Context2d get2DContext()****************** 
		 * Description
		 * 	Returns the 2D drawing context for this instance.
		 * Returns
		 * 	DISPLAY_CANVAS_INSTANCE.getContext("2D").
		 *********************************************************/
		DISPLAY_CANVAS_INSTANCE.get2DContext = function() {
			return DISPLAY_CANVAS_INSTANCE.getContext("2d");
		};

		/**public void resize(aspectRatio)************************* 		 
		 * Description
		 * 	Resizes this instance to as large as possible with 
		 * 	respect to this instance's parent element's size, and 
		 * 	the given aspect ratio.
		 *********************************************************/
		DISPLAY_CANVAS_INSTANCE.resize = function(aspectRatio) {
			/**TODO**********************************************
			 * 	Resize glitch.
			 ***************************************************/
			var w = 
				DISPLAY_CANVAS_INSTANCE.parentNode
				.clientWidth;
			var h = 
				DISPLAY_CANVAS_INSTANCE.parentNode
				.clientHeight;
			var finalWidth = 640;
			var finalHeight = 480;
			if(w / h >= aspectRatio) {
				finalHeight = h;
				finalWidth = Math.floor(h*aspectRatio);
			}
			else {
				finalWidth = w;
				finalHeight = Math.floor(w/aspectRatio);
			}
			DISPLAY_CANVAS_INSTANCE.width = finalWidth;
			DISPLAY_CANVAS_INSTANCE.height = finalHeight;
			DISPLAY_CANVAS_INSTANCE.style.padding 
				= Math.floor(
					(h - DISPLAY_CANVAS_INSTANCE.height)/2) 
				+ "px 0px 0px 0px ";
		}

		/*** Private Methods ***/
		/**private void init()*************************************
		 * Description
		 * 	Called at construction. During this time, the canvas 
		 * 	is styled and resized.
		 *********************************************************/
		function init() {
			if(!DISPLAY_CANVAS_INSTANCE) {
				throwError("NullPointerException",
					'document.getElementById("' 
					+ id + '") returned null.');
			}
			DISPLAY_CANVAS_INSTANCE.style.display = "block";
			DISPLAY_CANVAS_INSTANCE.style.margin = "0 auto";
			DISPLAY_CANVAS_INSTANCE.resize(4/3);
		}

		init();

		return DISPLAY_CANVAS_INSTANCE;
	}

	/**private class TimingThread()**********************************
	 * Description
	 * 	A thread used to sync-up drawing and updating.
	 * Returns
	 * 	A new instance of TimingThread.
	 ***************************************************************/
	function TimingThread() {

		/*** Private Variables ***/
		/**private int startTime***********************************
		 * Description
		 * 	The time in milliseconds the current interval 
		 * 	started.
		 *********************************************************/
		var startTime = parseInt(new Date().getTime());

		/**private int currentTime*********************************
		 * Description
		 * 	The current time in milliseconds.
		 *********************************************************/
		var currentTime = parseInt(new Date().getTime());

		/**private int lastUpdate**********************************
		 * Description
		 * 	The time in milliseconds of the last update.
		 *********************************************************/
		var lastUpdate = parseInt(new Date().getTime());

		/**private int lastDraw************************************
		 * Description
		 * 	The time in milliseconds of the last draw.
		 *********************************************************/
		var lastDraw = parseInt(new Date().getTime());

		/**private int updateCount*********************************
		 * Description
		 * 	The number of updates this interval.
		 *********************************************************/
		var updateCount = 0;

		/**private int frameCount**********************************
		 * Description
		 * 	The number of frames drawn this interval.
		 *********************************************************/
		var frameCount = 0;

		/**private int fps*****************************************
		 * Description
		 * 	The current frames per second.
		 *********************************************************/
		var fps = 0;

		/**private int ups*****************************************
		 * Description
		 * 	The current updates per second.
		 *********************************************************/
		var ups = 0;

		/*** Public Methods ***/
		/**public void start()*************************************
		 * Description
		 * 	Starts this instance.
		 *********************************************************/
		this.start = function() {
			/**TODO**********************************************
			 * 	Optimize.
			 ***************************************************/
			this.Loop = setInterval(function() {
				currentTime = parseInt(new Date().getTime());
				if(currentTime - startTime >= 1000) {
					resetInterval();
				}
				if((currentTime - lastUpdate) 
					>= (1000/targetUPS)
					&& updateCount < MAX_UPS) {
					update2D();
					lastUpdate = parseInt(new Date()
						.getTime());
					updateCount += 1;
				}
				if((currentTime - lastDraw) 
					>= (1000/targetFPS)
					&& frameCount < MAX_FPS) {
					if(lastDraw <= lastUpdate) {
						draw2D();
						render();
					}
					lastDraw = parseInt(new Date()
						.getTime());
				}
			}, 1000/500);
		}

		/**public void stop()**************************************
		 * Description
		 * 	Stops this instance.
		 *********************************************************/
		this.stop = function() {
			clearInterval(this.Loop);
		}

		/**public int getFPS()*************************************
		 * Description
		 * 	Returns the current frames per second.
		 * Returns
		 * 	fps
		 *********************************************************/
		this.getFPS = function() {
			return fps;
		}

		/**public int getUPS()*************************************
		 * Description
		 * 	Returns the current updates per second.
		 * Returns
		 * 	ups
		 *********************************************************/
		this.getUPS = function() {
			return ups;
		}

		/*** Private Methods ***/
		/**private void resetInterval()****************************
		 * Description
		 * 	Resets the timing interval.
		 *********************************************************/
		function resetInterval() {
			fps = frameCount;
			ups = updateCount;
			updateCount = 0;
			frameCount = 0;
			startTime = currentTime;
		}

		/**private void update2D()*********************************
		 * Description
		 * 	Updates the current 2D room.
		 *********************************************************/
		function update2D() {
			check2DCollision();
			room2D.onUpdate();
			for(var i=0;i < room2D.objects.length;i+=1) {
				if(room2D.objects[i].sprite) {
					if(room2D.objects[i].imageIndex 
						+ room2D
						.objects[i].imageSpeed 
						>= room2D.objects[i]
						.sprite.length) {
						room2D.objects[i]
							.imageIndex 
							= room2D
							.objects[i].imageIndex 
							+ room2D
							.objects[i].imageSpeed 
							- room2D.objects[i]
							.sprite.length;
					}
					else {
						room2D.objects[i].imageIndex 
							+= room2D
							.objects[i].imageSpeed;
					}
				}
				room2D.objects[i].x 
					+= room2D.objects[i].xSpeed;
				room2D.objects[i].y 
					+= room2D.objects[i].ySpeed;
				room2D.objects[i].z 
					+= room2D.objects[i].zSpeed;
				room2D.objects[i].r 
					+= room2D.objects[i].rSpeed;
				if(room2D.objects[i].depthScaling) {
					room2D.objects[i].scale 
						= (room2D.objects[i].z 
						/ 10000) + 1;
				}
				room2D.objects[i]
					.onUpdate();
			}
		}

		/**private void draw2D()***********************************
		 * Description
		 * 	Draws and renders the current 2D room.
		 *********************************************************/
		function draw2D() {
			var BlowFishG2D_Draw = DRAWING_CANVAS.get2DContext();
			BlowFishG2D_Draw.clearRect(0, 0, 
				BLOWFISH_INSTANCE.getCanvasWidth(), 
				BLOWFISH_INSTANCE.getCanvasHeight());
			room2D.onDraw(BlowFishG2D_Draw);
			var objects = sort2DByZIndex();
			for(var i=0;i<objects.length;i+=1) {
				if(objects[i].sprite && objects[i].visible) {
					BlowFishG2D_Draw.save();
					BlowFishG2D_Draw.translate(
						BlowFishG2D_Draw.getRoom().x 
						+ objects[i].x, 
						BlowFishG2D_Draw.getRoom().y 
						+ objects[i].y);
					BlowFishG2D_Draw.scale(
						objects[i].scale, 
						objects[i].scale);
					BlowFishG2D_Draw.translate(
						objects[i].width/2, 
						objects[i].height/2);
					BlowFishG2D_Draw.rotate(
						objects[i].r);
					BlowFishG2D_Draw.globalAlpha 
						= objects[i].imageAlpha;
					BlowFishG2D_Draw.drawImage(
						objects[i].sprite[
						Math.floor(objects[i].imageIndex)], 
						-(objects[i].width/2), 
						-(objects[i].height/2), 
						objects[i].width, 
						objects[i].height);
					BlowFishG2D_Draw.restore();
				}
				else if(objects[i].visible) {
					objects[i].onDraw(BlowFishG2D_Draw);
				}
			}
		}

		/**private void render()***********************************
		 * Description
		 * 	Renders the drawing canvas onto the display canvas.
		 *********************************************************/
		function render() {
			var BlowFishG2D = DISPLAY_CANVAS.get2DContext();
			var BlowFishG2D_Draw = DRAWING_CANVAS.get2DContext();
			BlowFishG2D.clearRect(0, 0, 
			BLOWFISH_INSTANCE.getDisplayWidth(), 
			BLOWFISH_INSTANCE.getDisplayHeight());
			BlowFishG2D.drawImage(BlowFishG2D_Draw.canvas, 0, 0, 
				BLOWFISH_INSTANCE.getDisplayWidth(), 
				BLOWFISH_INSTANCE.getDisplayHeight());
			frameCount += 1;
		}

		/**private BlowFishObject2D[] sort2DByZIndex()*************
		 * Description
		 * 	Sorts the objects in the 2D room by their z 
		 * 	coordinate from lowest to highest.
		 * Returns
		 * 	A sorted array.
	 	 *********************************************************/
		function sort2DByZIndex() {
			var objects = room2D.objects;
			var finalArray = new Array();
			var currentZ = -9999;
			while(finalArray.length < objects.length) {
				for(var i=0;i<objects.length;i+=1) {
					if(objects[i].z == currentZ) {
						finalArray[finalArray.length] 
							= objects[i];
					}
					else if(objects[i].z < -9999) {
						finalArray.push(objects[i]);
					}
					else if(objects[i].z > 9999) {
						finalArray.unshift(objects[i]);
					}
				}
				currentZ += 1;
			}
			return finalArray;
		}

		/**private void check2DCollision()*************************
		 * Description
		 * 	Checks for collisions in the current 2D room.
		 *********************************************************/
		function check2DCollision() {
			/**TODO**********************************************
			 * 	Precise collision checking.
			 ***************************************************/
			var objects = room2D.objects;
			for(var i=0;i<objects.length-1;i+=1) {
				for(var j=i+1;j<objects.length;j+=1) {
					var sameDepth = true;
					if(objects[j].depthTesting) {
						if(objects[j].z != objects[i].z) {
							sameDepth = false;
						}
					}
					if(sameDepth) {
						var L = (objects[i].x 
							+ objects[i].xSpeed 
							<= objects[j].x 
							+ (objects[j].width 
								* objects[j].scale)
							+ objects[j].xSpeed) 
							&& (objects[i].x 
							+ objects[i].xSpeed 
							> objects[j].x 
							+ objects[j].xSpeed);
						var R = (objects[i].x 
							+ (objects[i].width
								* objects[i].scale) 
							+ objects[i].xSpeed 
							>= objects[j].x 
							+ objects[j].xSpeed) 
							&& (objects[i].x 
							+ objects[i].xSpeed 
							< objects[j].x 
							+ objects[j].xSpeed);
						var T = (objects[i].y 
							+ objects[i].ySpeed 
							<= objects[j].y 
							+ (objects[j].height 
								* objects[j].scale)
							+ objects[j].ySpeed) 
							&& (objects[i].y 
							+ objects[i].ySpeed 
							> objects[j].y 
							+ objects[j].ySpeed);
						var B = (objects[i].y 
							+ (objects[i].height
 								* objects[i].scale)
							+ objects[i].ySpeed 
							>= objects[j].y 
							+ objects[j].ySpeed) 
							&& (objects[i].y 
							+ objects[i].ySpeed 
							< objects[j].y 
							+ objects[j].ySpeed);
						if((L || R) && (T || B)) {
							objects[j]
								.onCollision(
									objects[i]);
							objects[i]
								.onCollision(
									objects[j]);
						}
					}
				}
			}
		}
	}

	init();
}

/**public class BlowFishActivity()*************************************
 * Description
 * 	An application that runs on an instance of BlowFish.
 * Returns
 * 	A new instance of BlowFishActivity.
 *********************************************************************/
function BlowFishActivity() {

	/*** Private Variables ***/
	/**private static double TARGET_API_LEVEL************************
	 * Description
	 * 	The target API level of this instance. Used for backward 
	 * 	compatibility.
	 ***************************************************************/
	var TARGET_API_LEVEL = 1.0;

	/*** Public Methods ***/
	/**public void main()********************************************
	 * Description
	 * 	The entry point of this instance.
	 ***************************************************************/
	this.main = function(blowFish) {

	}

	/**public double getTargetAPILevel()*****************************
	 * Description
	 * 	Returns the target API level of this instance.
	 * Returns
	 * 	TARGET_API_LEVEL
	 ***************************************************************/
	this.getTargetAPILevel = function() {
		return TARGET_API_LEVEL;
	}

	/**public void setTargetAPILevel(level)**************************
	 * Description
	 * 	Sets the target API level of this instance.
	 * Parameters
	 * 	double level - The target API level for this instance.
	 ***************************************************************/
	this.setTargetAPILevel = function(level) {
		TARGET_API_LEVEL = level;
	}
}

/**public class BlowFishRoom2D(width, height)**************************
 * Description
 * 	A 2D container to store objects in.
 * Params
 * 	int width - The width of the room in pixels.
 * 	int height - The height of the room in pixels.
 * Returns
 * 	A new instance of BlowFishRoom2D.
 *********************************************************************/
function BlowFishRoom2D(width, height) {

	/*** Public Variables ***/
	/**public int x**************************************************
	 * Description
	 * 	The x coordinate of this instance in pixels.
	 ***************************************************************/
	this.x = 0;

	/**public int y**************************************************
	 * Description
	 * 	The y coordinate of this instance in pixels.
	 ***************************************************************/
	this.y = 0;

	/**public int width**********************************************
	 * Description
	 * 	The width of this instance in pixels.
	 ***************************************************************/
	if(width) {
		this.width = width;
	}
	else {
		this.width = 640;
	}

	/**public int height*********************************************
	 * Description
	 * 	The height of this instance in pixels.
	 ***************************************************************/
	if(height) {
		this.height = height;
	}
	else {
		this.height = 480;
	}

	/**public BlowFishObject2D[] objects*****************************
	 * Description
	 * 	An array of objects in the room.
	 ***************************************************************/
	this.objects = new Array();

	/*** Public Methods ***/
	/**public void add(blowFishObject2D)*****************************
	 * Description
	 * 	Adds an object to the room.
	 * Parameters
	 * 	blowFishObject2D - The object to be added to the room.
	 ***************************************************************/
	this.add = function(blowFishObject2D) {
		blowFishObject2D.onCreate();
		this.objects[this.objects.length] 
			= blowFishObject2D;
	}

	/**public void remove(blowFishObject2D)**************************
	 * Description
	 * 	Removes an object from the room.
	 * Parameters
	 * 	blowFishObject2D - The object to be removed from the room.
	 ***************************************************************/
	this.remove = function(blowFishObject2D) {
		blowFishObject2D.onDestroy();
		var index = -1;
		for(var i=0;i<this.objects.length;i+=1) {
			if(this.objects[i] === blowFishObject2D) {
				index = i;
				break;
			}
		}
		if(index > -1) {
			if(index != this.objects.length-1) {
				for(var i=index+1;i<this.objects.length;
					i+=1) {
					this.objects[i-1] 
						= this.objects[i];
				}
			}
			this.objects.pop();
		}
		else {
			return null;
		}
	}

	/**public void removeAll()***************************************
	 * Description
	 * 	Removes all objects from the room.
	 ***************************************************************/
	this.removeAll = function() {
		this.objects = new Array();
	}

	/**public BlowFishObject2D getObjectById(id)*********************
	 * Description
	 * 	Returns an object in the room with the given id.
	 * Parameters
	 * 	String id - The id of the desired object.
	 * Returns
	 * 	An object with the given id if an object is found. If an 
	 * 	object is not found, null is returned.
	 ***************************************************************/
	this.getObjectById = function(id) {
		var object = null;
		for(var i=0;i<this.objects.length;i+=1) {
			if(this.objects[i].id == id) {
				object = this.objects[i];
				break;
			}
		}
		return object;
	}

	/**public BlowFishObject2D[] getObjectsByClassName(className)****
	 * Description
	 * 	Finds all objects in the room with the given class.
	 * Parameters
	 * 	String className - A string representation of a class.
	 * Returns
	 * 	An object array of the given class.
	 ***************************************************************/
	this.getObjectsByClassName = function(className) {
		var objects = new Array();
		for(var i=0;i<this.objects.length;i+=1) {
			if(this.objects[i].className == className) {
				objects[objects.length] = this.objects[i];
			}
		}
		return objects;
	}

	/**public void onUpdate()****************************************
	 * Description
	 * 	The update event for this instance.
	 ***************************************************************/
	this.onUpdate = function() {

	}

	/**public void onDraw()******************************************
	 * Description
	 * 	The draw event for this instance.
	 ***************************************************************/
	this.onDraw = function(BlowFishG2D) {

	}
}

/**public class BlowFishRoom3D(width, height, depth)*******************
 * Description
 * 	A 3D container to store objects in.
 * Params
 * 	int width - The width of the room in pixels.
 * 	int height - The height of the room in pixels.
 * 	int depth - The depth of the room in pixels.
 * Returns
 * 	A new instance of BlowFishRoom3D.
 *********************************************************************/
function BlowFishRoom3D(width, height, depth) {
	/**TODO**********************************************************
	 * 	Unimplemented.
	 ***************************************************************/
}

/**public class BlowFishObject2D(sprite, x, y, z)**********************
 * Description
 * 	A 2D, drawable, dynamic object.
 * Parameters
 * 	BlowFishResources.Sprite sprite - The image resource(s) for this 
 * 		object.
 * 	int x - X coordinate in pixels.
 * 	int y - Y coordinate in pixels.
 * 	int z - Z coordinate in pixels.
 * Returns
 * 	A new instance of BlowFishObject2D.
 *********************************************************************/
function BlowFishObject2D(sprite, x, y, z) {

	/*** Public Variables ***/
	/**public String id**********************************************
	 * Description
	 * 	A unique id.
	 ***************************************************************/
	this.id = "undefined";

	/**public String class*******************************************
	 * Description
	 * 	A string representation of this class.
	 ***************************************************************/
	this.className = "BlowFishObject2D";

	/**public boolean visible****************************************
	 * Description
	 * 	Used to determine if the object should be drawn.
	 ***************************************************************/
	this.visible = true;

	/**public boolean depthTesting***********************************
	 * Description
	 * 	Used to determine if the objects z position should be taken 
	 * 	into account when checking for collisions.
	 ***************************************************************/
	this.depthTesting = false;

	/**public boolean depthScaling***********************************
	 * Description
	 * 	Used to determine if the objects z position should 
	 * 	determine the scale of the object.
	 ***************************************************************/
	this.depthScaling = false;

	/**public BlowFishResources.Sprite sprite************************
	 * Description
	 * 	A set of images.
	 ***************************************************************/
	this.sprite = sprite;

	/**public int imageIndex*****************************************
	 * Description
	 * 	The current frame of the sprite to be drawn.
	 ***************************************************************/
	this.imageIndex = 0;

	/**public double imageSpeed**************************************
	 * Description
	 * 	The speed of the sprite animation in decimal-percent.
	 ***************************************************************/
	this.imageSpeed = 1.0;

	/**public double imageAlpha**************************************
	 * Description
	 * 	The alpha (transparency) of the sprite.
	 ***************************************************************/
	this.imageAlpha = 1.0;

	/**public double scale*******************************************
	 * Description
	 * 	The scale of this instance.
	 ***************************************************************/
	this.scale = 1.0;

	/**public int xSpeed*********************************************
	 * Description
	 * 	The horizontal speed of this instance in pixels.
	 ***************************************************************/
	this.xSpeed = 0;

	/**public int ySpeed*********************************************
	 * Description
	 * 	The vertical speed of this instance in pixels.
	 ***************************************************************/
	this.ySpeed = 0;

	/**public int zSpeed*********************************************
	 * Description
	 * 	The depth speed of this instance in pixels.
	 ***************************************************************/
	this.zSpeed = 0;

	/**public double rSpeed******************************************
	 * Description
	 * 	The rotation speed of this instance in radians.
	 ***************************************************************/
	this.rSpeed = 0.0;

	/**public int width**********************************************
	 * Description
	 * 	The width of this instance.
	 ***************************************************************/
	if(sprite) {
		var s = sprite[0];
		for(var i=1;i<sprite.length;i+=1) {
			if(sprite[i].width > s.width) {
				s = sprite[i];
			}
		}
		this.width = s.width;
	}
	else {
		this.width = 0;
	}

	/**public int height*********************************************
	 * Description
	 * 	The height of this instance.
	 ***************************************************************/
	if(sprite) {
		var s = sprite[0];
		for(var i=1;i<sprite.length;i+=1) {
			if(sprite[i].height > s.height) {
				s = sprite[i];
			}
		}
		this.height = s.height;
	}
	else {
		this.height = 0;
	}

	/**public int x**************************************************
	 * Description
	 * 	The x coordinate of this instance's top, left corner, in 
	 * 	pixels.
	 ***************************************************************/
	if(x) {
		this.x = x;
	}
	else {
		this.x = 0;
	}

	/**public int y**************************************************
	 * Description
	 * 	The y coordinate of this instance's top, left corner, in 
	 * 	pixels.
	 ***************************************************************/
	if(y) {
		this.y = y;
	}
	else {
		this.y = 0;
	}

	/**public int z**************************************************
	 * Description
	 * 	The z coordinate (depth) of this instance's top, left 
	 * 	corner, in pixels.
	 ***************************************************************/
	if(z) {
		this.z = z;
	}
	else {
		this.z = 0;
	}

	/**public double r***********************************************
	 * Description
	 * 	The rotation of this instance, in radians.
	 ***************************************************************/
	this.r = 0.0;

	/*** Public Methods ***/
	/**public void onUpdate()****************************************
	 * Description
	 * 	The update event for this instance.
	 ***************************************************************/
	this.onUpdate = function() {

	}

	/**public void onDraw()******************************************
	 * Description
	 * 	The draw event for this instance.
	 ***************************************************************/
	this.onDraw = function(BlowFishG2D) {

	}

	/**public void onCreate()****************************************
	 * Description
	 * 	The creation event for this instance. Called when this 
	 * 	instance is added to a room.
	 ***************************************************************/
	this.onCreate = function() {

	}

	/**public void onDestroy()***************************************
	 * Description
	 * 	The destruction event for this instance. Called when this 
	 * 	instance is removed from a room.
	 ***************************************************************/
	this.onDestroy = function() {

	}

	/**public void onCollision(object)*******************************
	 * Description
	 * 	The collision event for this instance.
	 * Parameters
	 * 	BlowFishObject2D object - The collision object.
	 ***************************************************************/
	this.onCollision = function(object) {

	}

	/**public void onMouseDown(x, y, button)*************************
	 * Description
	 * 	The mouse-down event for this instance. Called when a mouse 
	 * 	button is down.
	 * Parameters
	 * 	int x - The x coordinate of the mouse in pixels.
	 * 	int y - The y coordinate of the mouse in pixels.
	 * 	int button - The button pressed.
	 ***************************************************************/
	this.onMouseDown = function(x, y, button) {

	}

	/**public void onMouseClick(x, y, button)************************
	 * Description
	 * 	The mouse-click event for this instance. Called when a 
	 * 	mouse button is first pressed down.
	 * Parameters
	 * 	int x - The x coordinate of the mouse in pixels.
	 * 	int y - The y coordinate of the mouse in pixels.
	 * 	int button - The button clicked.
	 ***************************************************************/
	this.onMouseClick = function(x, y, button) {

	}

	/**public void onMouseDoubleClick(x, y)**************************
	 * Description
	 * 	The mouse-double-click event for this instance.
	 * Parameters
	 * 	int x - The x coordinate of the mouse in pixels.
	 * 	int y - The y coordinate of the mouse in pixels.
	 ***************************************************************/
	this.onMouseDoubleClick = function(x, y) {

	}

	/**public void onMouseUp(x, y, button)***************************
	 * Description
	 * 	The mouse-up event for this instance. Called when a mouse 
	 * 	button is released.
	 * Parameters
	 * 	int x - The x coordinate of the mouse in pixels.
	 * 	int y - The y coordinate of the mouse in pixels.
	 * 	int button - The button released.
	 ***************************************************************/
	this.onMouseUp = function(x, y, button) {

	}

	/**public void onMouseOver(x, y)*********************************
	 * Description
	 * 	The mouse-over event for this instance. Called when the 
	 * 	mouse begins hovering over this instance.
	 * Parameters
	 * 	int x - The x coordinate of the mouse in pixels.
	 * 	int y - The y coordinate of the mouse in pixels.
	 ***************************************************************/
	this.onMouseOver = function(x, y) {

	}

	/**public void onMouseHover(x, y)********************************
	 * Description
	 * 	The mouse-hover event for this instance. Called when the 
	 * 	mouse is over this instance.
	 * Parameters
	 * 	int x - The x coordinate of the mouse in pixels.
	 * 	int y - The y coordinate of the mouse in pixels.
	 ***************************************************************/
	this.onMouseHover = function(x, y) {

	}

	/**public void onMouseOut(x, y)**********************************
	 * Description
	 * 	The mouse-out event for this instance. Called when the 
	 * 	mouse stops hovering over this instance.
	 * Parameters
	 * 	int x - The x coordinate of the mouse in pixels.
	 * 	int y - The y coordinate of the mouse in pixels.
	 ***************************************************************/
	this.onMouseOut = function(x, y) {

	}

	/**public void onKeyDown(key)************************************
	 * Description
	 * 	The key-down event for this instance. Called when a key is 
	 * 	down.
	 * Parameters
	 * 	int key - The key-code of the key down.
	 ***************************************************************/
	this.onKeyDown = function(key) {

	}

	/**public void onKeyPress(key)***********************************
	 * Description
	 * 	The key-press event for this instance. Called when a key is 
	 * 	first pressed down.
	 * Parameters
	 * 	int key - The key-code of the key pressed.
	 ***************************************************************/
	this.onKeyPress = function(key) {

	}

	/**public void onKeyUp(key)**************************************
	 * Description
	 * 	The key-up event for this instance. Called when a key is 
	 * 	released.
	 * Parameters
	 * 	int key - The key-code of the key released.
	 ***************************************************************/
	this.onKeyUp = function(key) {

	}
}

/**public class BlowFishObject3D(model, x, y, z)***********************
 * Description
 * 	A 3D, drawable, dynamic object.
 * Parameters
 * 	BlowFishResources.Model model - The visual resource(s) for this 
 * 		object.
 * 	int x - X coordinate in pixels.
 * 	int y - Y coordinate in pixels.
 * 	int z - Z coordinate in pixels.
 * Returns
 * 	A new instance of BlowFishObject3D.
 *********************************************************************/
function BlowFishObject3D(model, x, y, z) {
	/**TODO**********************************************************
	 * 	Unimplemented.
	 ***************************************************************/
}

/**public class BlowFishResources()************************************
 * Description
 * 	A container that creates, loads, and returns audio-visual 
 * 	resources.
 * Returns
 * 	A new instance of BlowFishResources.
 *********************************************************************/
function BlowFishResources() {

	/*** Private Variables ***/
	/**private static final BlowFishResources RESOURCES_INSTANCE*****
	 * Description
	 * 	A handle to this instance.
 	 ***************************************************************/
	var RESOURCES_INSTANCE = this;

	/**private Sprite[] sprites**************************************
	 * Description
	 * 	An array of sprites.
 	 ***************************************************************/
	var sprites = new Array();

	/**private Sound[] sounds****************************************
	 * Description
	 * 	An array of sounds.
 	 ***************************************************************/
	var sounds = new Array();
	
	/*** Public Methods ***/
	/**public int addSprite(src)************************************
	 * Description
	 * 	Adds a sprite resource to the sprite array.
	 * Parameters
	 * 	String[] src - The paths to the image(s).
	 * Returns
	 * 	The index of the new sprite.
 	 ***************************************************************/
	this.addSprite = function(src) {
		sprites[sprites.length] = new Sprite(src);
		return sprites.length - 1;
	}

	/**public Sprite getSprite(index)********************************
	 * Description
	 * 	Returns the sprite at the given index in the sprite array.
	 * Parameters
	 * 	int index - The index of the desired sprite.
	 * Returns
	 * 	The sprite at the given index.
 	 ***************************************************************/
	this.getSprite = function(index) {
		return sprites[index];
	}

	/**public int addSound(src, loop)*****************************
	 * Description
	 * 	Adds a sound resource to the sound array.
	 * Parameters
	 * 	String src - The source of the sound.
	 * Returns
	 * 	The index of the newly added sound.
 	 ***************************************************************/
	this.addSound = function(src) {
		sounds[sounds.length] = new Sound(src);
		return sounds.length - 1;
	}

	/**public Sound getSound(index)**********************************
	 * Description
	 * 	Returns the sound at the given index in the sounds array.
	 * Parameters
	 * 	int index - The index of the sound.
	 * Returns
	 * 	The sound at the given index.
 	 ***************************************************************/
	this.getSound = function(index) {
		return sounds[index];
	}

	/**public void load()*******************************************
	 * Description
	 * 	Loads the resources.
 	 ***************************************************************/
	this.load = function() {
		for(var i=0;i<sounds.length;i+=1) {
			sounds[i].load();
		}
		for(var i=0;i<sprites.length;i+=1) {
			var s = sprites[i];
			for(var j=0;j<s.length;j+=1) {
				s[j].load();
			}
		}
	}

	/**public void purge()*******************************************
	 * Description
	 * 	Empties the sprite and sound arrays.
 	 ***************************************************************/
	this.purge = function() {
		sprites = new Array();
		sounds = new Array();
	}

	/**public void onProgress(percentLoaded)*************************
	 * Description
	 * 	The progress event for this instance. Called when loading 
	 * 	progress has been made.
	 * Parameters
	 * 	percentLoaded - The loading progress of this instance in 
	 * 		percent.
 	 ***************************************************************/
	this.onProgress = function(percentLoaded) {

	}

	/**public void onLoad()******************************************
	 * Description
	 * 	The load event for this instance. Called when all resources 
	 * 	have finished loading.
 	 ***************************************************************/
	this.onLoad = function() {

	}

	/**public void onError(event)************************************
	 * Description
	 * 	The error event for this instance.
	 * Parameters
	 * 	Object event - An event object, containing an error code 
	 * 		and a message. The error code can be accessed through 
	 * 		event.code and the error message can be accessed 
	 * 		through event.message.
	 ***************************************************************/
	this.onError = function(event) {
		window.alert(event.type + ": " + event.message);
	}

	/*** Private Methods ***/
	/**private int calculateProgress()*******************************
	 * Description
	 * 	Calculates the loading progress of this instance.
	 * Returns
	 * 	The average of the progress for each sprite and sound.
 	 ***************************************************************/
	function calculateProgress() {
		var loaded = 0;
		var total = 0;
		for(var i=0;i<sprites.length;i+=1) {
			var s = sprites[i];
			for(var j=0;j<s.length;j+=1) {
				loaded += s[j].bytesLoaded;
				total += s[j].bytesTotal;
			}
		}
		for(var i=0;i<sounds.length;i+=1) {
			loaded += sounds[i].bytesLoaded;
			total += sounds[i].bytesTotal;
		}

		var progress = loaded / total * 100;

		RESOURCES_INSTANCE.onProgress(progress);
		if(progress == 100) {
			setTimeout(function() {
				RESOURCES_INSTANCE.onLoad();
			}, 250);
		}
	}

	/**private void throwError(type, message)************************
	 * Description
	 * 	Throws an error with the given type and message.
	 * Parameters
	 * 	int code - An error code.
	 * 	String message - An error message.
	 ***************************************************************/
	function throwError(type, message) {
		var o = new Object();
		o.type = type;
		o.message = message;
		RESOURCES_INSTANCE.onError(o);
	}

	/**private String encodeUint8ArrayToBase64(input)****************
	 * Description
	 * 	Encodes an unsigned integer array to a base-64 url.
	 * Parameters
	 * 	Uint8Array input - The bytes to be encoded.
	 * Returns
	 * 	An encoded string.
	 ***************************************************************/
	function encodeUint8ArrayToBase64(input) {
    		var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    		var output = "";
    		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    		var i = 0;

    		while (i < input.length) {
        		chr1 = input[i++];
        		chr2 = i < input.length ? input[i++] : Number.NaN; // Not sure if the index 
        		chr3 = i < input.length ? input[i++] : Number.NaN; // checks are needed here

        		enc1 = chr1 >> 2;
        		enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        		enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        		enc4 = chr3 & 63;

        		if (isNaN(chr2)) {
            		enc3 = enc4 = 64;
        		} else if (isNaN(chr3)) {
           			enc4 = 64;
        		}
        		output += keyStr.charAt(enc1) + keyStr.charAt(enc2) +
				keyStr.charAt(enc3) + keyStr.charAt(enc4);
		}

		return output;
	}

	/*** Private Classes ***/
	/**private class Sprite(src) extends Array***********************
	 * Description
	 * 	An array of images.
	 * Parameters
	 * 	String[] src - An array of image source paths.
	 * Returns
	 * 	A new instance of Sprite.
	 * Throws
	 * 	NetworkError(404) - If the image(s) cannot be found.
	 * 	UnsupportedException - If the source(s) supplied are not 
	 * 		supported.
	 ***************************************************************/
	function Sprite(src) {

		/*** Private Variables ***/
		/**private Image[] SPRITE_INSTANCE*************************
		 * Description
		 * 	A handle to the prototype for this instance.
		 *********************************************************/
		var SPRITE_INSTANCE = new Array(src.length);

		/*** Private Methods ***/
		/**private void init()*************************************
		 * Description
		 * 	Called at construction. During this time, each of the 
		 * 	frames are initialized.
		 *********************************************************/
		function init() {
			for(var i=0;i<src.length;i+=1) {
				SPRITE_INSTANCE[i] = new SpriteFrame(src[i]);
			}
		}

		/*** Private Classes ***/
		/**private class SpriteFrame(src) extends Image************
		 * Description
		 * 	A single frame of a sprite.
		 * Parameters
		 * 	String src - The source of the image.
		 * Returns
		 * 	A new instance of SpriteFrame.
		 * Throws
		 * 	NetworkError(404) - If the image cannot be found.
		 * 	UnsupportedException - If the source supplied is not 
		 * 		supported.
		 *********************************************************/
		function SpriteFrame(src) {

			/*** Private Variables ***/
			/**private Image SPRITE_FRAME_INSTANCE***************
			 * Description
			 * 	A handle to the prototype for this instance.
			 ***************************************************/
			var SPRITE_FRAME_INSTANCE = new Image();

			/**private String type*******************************
			 * Description
			 * 	The type supplied.
			 ***************************************************/
			var type = "";

			/*** Public Variables ***/
			/**public int bytesLoaded****************************
			 * Description
			 * 	The number of bytes loaded.
			 ***************************************************/
			SPRITE_FRAME_INSTANCE.bytesLoaded = 0;

			/**public int bytesTotal*****************************
			 * Description
			 * 	The total number of bytes.
			 ***************************************************/
			SPRITE_FRAME_INSTANCE.bytesTotal = 999999;

			/*** Public Methods ***/
			/**public void load**********************************
			 * Description
			 * 	Loads the image.
			 * Throws
			 * 	NetworkError(404) - If the image cannot be 
			 * 		found.
			 * 	UnsupportedException - If the image type is 
			 * 		not supported.
			 ***************************************************/
			SPRITE_FRAME_INSTANCE.load = function(){
				if(src.indexOf(".bmp") > -1) {
					type = "image/bmp";
				}
				else if(src.indexOf(".png") > -1) {
					type = "image/png";
				}
				else if(src.indexOf(".jpg") > -1) {
					type = "image/jpg";
				}
				else if(src.indexOf(".gif") > -1) {
					type = "image/gif";
				}
				else {
					throwError("UnsupportedException",
						src 
						+ "; Image type not supported.");
				}
				var thisImg = this;
				
				var blobSupport = true;
				try {
					var b = Blob;
				}
				catch(e) {
					blobSupport = false;
				}
				var uint8ArraySupport = true;
				try {
					var u = Uint8Array;
				}
				catch(e) {
					uint8ArraySupport = false;
				}

				if(blobSupport || uint8ArraySupport) {
					var xmlHTTP = new XMLHttpRequest();
					xmlHTTP.open("GET", src, true);
					xmlHTTP.responseType = "arraybuffer";
					xmlHTTP.onload = function(e) {
						try{
							var blob 
								= new Blob([this.response],
								{type: type});
							thisImg.src 
								= window.URL
								.createObjectURL(blob);
						}
						catch(e){
							thisImg.src = "data:" 
								+ type 
								+ ";base64," 
								+ encodeUint8ArrayToBase64(
								new Uint8Array(this.response));
						}
					};
					xmlHTTP.onprogress = function(e) {
						thisImg.bytesLoaded = e.loaded;
						thisImg.bytesTotal = e.total;
						calculateProgress();
					};
					xmlHTTP.onloadstart = function() {
						thisImg.bytesLoaded = 0;
					};
					xmlHTTP.onerror = function() {
						throwError("NetworkError(404)",
							src 
							+ '" could not be found.');
					}
					xmlHTTP.send();
				}
				else {
					thisImg.onload = function() {
						thisImg.bytesLoaded = thisImg.bytesTotal;
						calculateProgress();
					}
					thisImg.src = src;
				}
			};

			return SPRITE_FRAME_INSTANCE;
		}

		init();

		return SPRITE_INSTANCE;
	}

	/**private class Sound(src, loop) extends Audio******************
	 * Description
	 * 	A sound resource.
	 * Parameters
	 * 	String[] src - The source path(s) to the sound. Multiple 
	 *		formats may be supplied.
	 * 	boolean loop - If set to true, the sound will loop.
	 * Returns
	 * 	A new instance of Sound.
	 * Throws
	 * 	NetworkError(404) - If the sound cannot be found.
	 * 	UnsupportedException - If the sound type(s) supplied are 
	 * 		not supported by BlowFish or the browser does not 
	 * 		support the supplied sound type(s).
	 ***************************************************************/
	function Sound(src) {

		/*** Private Variables ***/
		/**private static final Audio SOUND_INSTANCE***************
		 * Description
		 * 	A handle to the prototype for this instance.
		 *********************************************************/
		try {
			var SOUND_INSTANCE = new Audio();
		}
		catch(e) {
			throwError("UnsupportedException",
				"Your browser does not support HTML5 Audio.");
		}
		

		/*** Public Variables ***/
		/**public int bytesLoaded*********************************
		 * Description
		 * 	The number of bytes loaded.
		 *********************************************************/
		SOUND_INSTANCE.bytesLoaded = 0;

		/**public int bytesTotal**********************************
		 * Description
		 * 	The total of bytes loaded.
		 *********************************************************/
		SOUND_INSTANCE.bytesTotal = 999999;

		/*** Public Methods ***/
		/**public Sound clone()************************************
		 * Description
		 * 	Returns a clone of the sound.
		 * Returns
		 * 	A clone of this instance.
		 *********************************************************/
		SOUND_INSTANCE.clone = function() {
			clone = SOUND_INSTANCE.cloneNode(true);
			clone.currentTime = 0;
			clone.volume = this.volume;
			return clone;
		}

		/*** Private Methods ***/
		/**private void load()*************************************
		 * Description
		 * 	Loads the sound.
		 *********************************************************/
		function load() {
			var supported = false;
			var supportedSource = -1;
			var type = "";
			for(var i=0;i<src.length;i+=1) {
				if(src[i].indexOf(".mp3") > -1) {
					type = "audio/mpeg";
				}
				else if(src[i].indexOf(".wav") > -1) {
					type = "audio/wav";
				}
				else if(src[i].indexOf(".ogg") > -1) {
					type = "audio/ogg";
				}
				else {
					throwError("UnsupportedException",
						src[i] 
						+ "; Sound type not supported"
						+ " by BlowFish.");
				}
				if(SOUND_INSTANCE.canPlayType(type)) {
					supported = true;
					supportedSource = i;
					break;
				}
			}
			if(!supported) {
				throwError("UnsupportedException",
					"Your browser does not support" 
					+ " the supplied audio formats.");
			}
			else {
				var thisSound = this;
				var xmlHTTP = new XMLHttpRequest();
				xmlHTTP.open("GET", 
					src[supportedSource], 
					true);
				xmlHTTP.responseType = "arraybuffer";
				xmlHTTP.onload = function(e) {
					try {
						var blob 
							= new Blob([this.response],
							{type: type});
						thisSound.src 
							= window.URL
							.createObjectURL(blob);
					}
					catch(e){
						thisSound.src = "data:" 
							+ type 
							+ ";base64," 
							+ encodeUint8ArrayToBase64(
							new Uint8Array(
								this.response));
					}
				};
				xmlHTTP.onprogress = function(e) {
					thisSound.bytesLoaded = e.loaded;
					thisSound.bytesTotal = e.total;
					calculateProgress();
				};
				xmlHTTP.onloadstart = function() {
					thisSound.bytesLoaded = 0;
				};
				xmlHTTP.send();
			}
		}

		/**private void init()*************************************
		 * Description
		 * 	Initializes the instance.
		 *********************************************************/
		function init() {
			var blobSupport = true;
			try {
				var b = Blob;
			}
			catch(e) {
				blobSupport = false;
			}
			var uint8ArraySupport = true;
			try {
				var u = Uint8Array;
			}
			catch(e) {
				uint8ArraySupport = false;
			}

			if(blobSupport || uint8ArraySupport) {
				SOUND_INSTANCE.load = load;
			}
			else {
				var supported = false;
				var supportedSource = -1;
				var type = "";
				for(var i=0;i<src.length;i+=1) {
					if(src[i].indexOf(".mp3") > -1) {
						type = "audio/mpeg";
					}
					else if(src[i].indexOf(".wav") > -1) {
						type = "audio/wav";
					}
					else if(src[i].indexOf(".ogg") > -1) {
						type = "audio/ogg";
					}
					else {
						throwError("UnsupportedException",
							src[i] 
							+ "; Sound type not supported"
							+ " by BlowFish.");
					}
					if(SOUND_INSTANCE.canPlayType(type)) {
						supported = true;
						supportedSource = i;
						break;
					}
				}
				if(!supported) {
					throwError("UnsupportedException",
						"Your browser does not support" 
						+ " the supplied audio formats.");
				}
				else {
					SOUND_INSTANCE.onloadeddata = function() {
						SOUND_INSTANCE.bytesLoaded = SOUND_INSTANCE.bytesTotal;
						calculateProgress();
					}
					SOUND_INSTANCE.preload = false;
					var source = document.createElement('source');
					source.type = type;
					source.src = src[supportedSource];
					SOUND_INSTANCE.appendChild(source);
				}
			}
		}

		init();

		return SOUND_INSTANCE;
	}
}