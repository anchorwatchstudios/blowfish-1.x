/*** Public Classes ***/
function UnknownSpace() {
	/*** Private Variables ***/
	var UNKNOWNSPACE_INSTANCE = new BlowFishActivity();
	var BLOWFISH_INSTANCE;
	var GLOBAL_R = new BlowFishResources();
	var R = new BlowFishResources();
	
	/*** Public Methods ***/
	UNKNOWNSPACE_INSTANCE.main = function(blowFish) {
		BLOWFISH_INSTANCE = blowFish;
		BLOWFISH_INSTANCE.setCanvasWidth(853);
		BLOWFISH_INSTANCE.setCanvasHeight(480);
		loadMainMenu();
	}

	/*** Private Methods ***/
	function loadMainMenu() {
		GLOBAL_R.onLoad = function() {
			loadPrototypeLevel();
		}
		GLOBAL_R.SPRITE_BACKGROUND_1 = GLOBAL_R.addSprite(new Array(
			"Images/background.png",
			"Images/midground.png",
			"Images/foreground.png"
		));
		GLOBAL_R.load();
	}

	function loadPrototypeLevel() {
		var loadingScreen = new LoadingScreen();
		BLOWFISH_INSTANCE.set2DRoom(loadingScreen);
		R.onProgress = function(progress) {
			loadingScreen.progress = Math.floor(progress);
		}
		R.onLoad = function() {
			BLOWFISH_INSTANCE.set2DRoom(new PrototypeLevel());
			document.getElementById("blowFish").ontouchstart = function() {
				BLOWFISH_INSTANCE.get2DRoom().getObjectsByClassName("Player")[0].onKeyDown(38);
			}
		}

		R.SPRITE_ASTEROID_1 = R.addSprite(new Array(
			"Images/asteroid0.png",
			"Images/asteroid1.png",
			"Images/asteroid2.png",
			"Images/asteroid3.png",
			"Images/asteroid4.png",
			"Images/asteroid5.png",
			"Images/asteroid6.png",
			"Images/asteroid7.png",
			"Images/asteroid8.png",
			"Images/asteroid9.png",
			"Images/asteroid10.png",
			"Images/asteroid11.png",
			"Images/asteroid12.png",
			"Images/asteroid13.png",
			"Images/asteroid14.png",
			"Images/asteroid15.png"
		));
		R.SPRITE_LUNA = R.addSprite(new Array(
			"Images/luna0.png",
			"Images/luna1.png",
			"Images/luna2.png",
			"Images/luna3.png",
			"Images/luna4.png",
			"Images/luna5.png",
			"Images/luna6.png",
			"Images/luna7.png",
			"Images/luna8.png",
			"Images/luna9.png",
			"Images/luna10.png",
			"Images/luna11.png",
			"Images/luna12.png",
			"Images/luna13.png",
			"Images/luna14.png",
			"Images/luna15.png"
		));
		R.SPRITE_EMBLEM = R.addSprite(new Array(
			"Images/emblem.png"
		));

		R.SOUND_SONG1 = R.addSound(new Array("Audio/ASpaceUnknown.mp3", 
			"Audio/ASpaceUnknown.ogg",
			"Audio/ASpaceUnknown.wav"));
		R.getSound(R.SOUND_SONG1).loop = true;

		R.load();
	}

	/*** Private Classes ***/
	function Player(x, y) {

		/*** Private Variables ***/
		var PLAYER_INSTANCE = new Ship();

		/*** Public Variables ***/
		PLAYER_INSTANCE.className = "Player";
		PLAYER_INSTANCE.x = x;
		PLAYER_INSTANCE.y = y;

		/*** Public Methods ***/		
		PLAYER_INSTANCE.onKeyDown = function(key) {
			if(key == 38) {
				//Thrust forward
				var direction = calculateQuadraticDirection();
				var degrees = radiansToDegrees(PLAYER_INSTANCE.r) % 360;
				if(degrees < 0) {
					degrees += 360;
				}
				degrees -= direction * 90;

				if(direction == 0) {
					if(Math.abs(this.xSpeed + this.acceleration)  < this.MAX_SPEED) {
						this.xSpeed += this.acceleration * (degrees/90);
					}
					if(Math.abs(this.ySpeed - this.acceleration) < this.MAX_SPEED) {
						this.ySpeed -= this.acceleration * ((90 - degrees)/90);
					}
				}
				else if(direction == 1) {
					if(Math.abs(this.xSpeed + this.acceleration) < this.MAX_SPEED) {
						this.xSpeed += this.acceleration * ((90 - degrees)/90);
					}
					if(Math.abs(this.ySpeed + this.acceleration) < this.MAX_SPEED) {
						this.ySpeed += this.acceleration * (degrees/90);
					}
				}
				else if(direction == 2) {
					if(Math.abs(this.xSpeed - this.acceleration) < this.MAX_SPEED) {
						this.xSpeed -= this.acceleration * (degrees/90);
					}
					if(Math.abs(this.ySpeed + this.acceleration) < this.MAX_SPEED) {
						this.ySpeed += this.acceleration * ((90 - degrees)/90);
					}
				}
				else if(direction == 3) {
					if(Math.abs(this.xSpeed - this.acceleration) < this.MAX_SPEED) {
						this.xSpeed -= this.acceleration * ((90 - degrees)/90);
					}
					if(Math.abs(this.ySpeed - this.acceleration) < this.MAX_SPEED) {
						this.ySpeed -= this.acceleration * (degrees/90);
					}
				}
			}
			else if(key == 40) {
				//Thrust backward
				var direction = calculateQuadraticDirection();
				var degrees = radiansToDegrees(PLAYER_INSTANCE.r) % 360;
				if(degrees < 0) {
					degrees += 360;
				}
				degrees -= direction * 90;

				if(direction == 0) {
					if(Math.abs(this.xSpeed - this.acceleration) < this.MAX_SPEED) {
						this.xSpeed -= this.acceleration * (degrees/90);
					}
					if(Math.abs(this.ySpeed + this.acceleration) < this.MAX_SPEED) {
						this.ySpeed += this.acceleration * ((90 - degrees)/90);
					}
				}
				else if(direction == 1) {
					if(Math.abs(this.xSpeed - this.acceleration) < this.MAX_SPEED) {
						this.xSpeed -= this.acceleration * ((90 - degrees)/90);
					}
					if(Math.abs(this.ySpeed - this.acceleration) < this.MAX_SPEED) {
						this.ySpeed -= this.acceleration * (degrees/90);
					}
				}
				else if(direction == 2) {
					if(Math.abs(this.xSpeed + this.acceleration) < this.MAX_SPEED) {
						this.xSpeed += this.acceleration * (degrees/90);
					}
					if(Math.abs(this.ySpeed - this.acceleration) < this.MAX_SPEED) {
						this.ySpeed -= this.acceleration * ((90 - degrees)/90);
					}
				}
				else if(direction == 3) {
					if(Math.abs(this.xSpeed + this.acceleration) < this.MAX_SPEED) {
						this.xSpeed += this.acceleration * ((90 - degrees)/90);
					}
					if(Math.abs(this.ySpeed + this.acceleration) < this.MAX_SPEED) {
						this.ySpeed += this.acceleration * (degrees/90);
					}
				}
			}
		}

		PLAYER_INSTANCE.onKeyPress = function(key) {
			if(key == 37) {
				this.rSpeed = -.1;
			}
			else if(key == 39) {
				this.rSpeed = .1;
			}
			else if(key == 32) {
				this.imageSpeed = .5;
			}
			else if(key == 220) {
				if(BLOWFISH_INSTANCE.isFullScreen()) {
					BLOWFISH_INSTANCE.restoreScreen();
				}
				else {
					BLOWFISH_INSTANCE.fullScreen();
				}
			}
		}

		PLAYER_INSTANCE.onKeyUp = function(key) {
			if(key == 37 || key == 39) {
				this.rSpeed = 0;
			}
		}

		/*** Private Methods ***/
		function calculateQuadraticDirection() {
			var direction;
			var degrees = radiansToDegrees(PLAYER_INSTANCE.r) % 360;
			if(degrees < 0) {
				degrees += 360;
			}
			if(degrees >=0 && degrees < 90) {
				direction = 0;
			}
			else if(degrees >=90 && degrees < 180) {
				direction = 1;
			}
			else if(degrees >=180 && degrees < 270) {
				direction = 2;
			}
			else {
				direction = 3;
			}
			return direction;
		}

		function radiansToDegrees(rad) {
			return rad * 57.2957795;
		}

		return PLAYER_INSTANCE;
	}

	function Enemy() {
		var ENEMY_INSTANCE = new Ship();
		return ENEMY_INSTANCE;
	}

	function AI() {
		var AI_INSTANCE = new Ship();
		return AI_INSTANCE;
	}

	function Ship() {
		var SHIP_INSTANCE = new BlowFishObject2D(
			R.getSprite(R.SPRITE_LUNA));
		SHIP_INSTANCE.className = "Ship";
		SHIP_INSTANCE.imageSpeed = 0;
		SHIP_INSTANCE.x = 0;
		SHIP_INSTANCE.y = 0;
		SHIP_INSTANCE.acceleration = .6;
		SHIP_INSTANCE.MAX_SPEED = 10;

		SHIP_INSTANCE.onUpdate = function() {
			var room = BLOWFISH_INSTANCE.get2DRoom();
			room.x += -this.xSpeed;
			room.y += -this.ySpeed;
			if(this.imageSpeed != 0) {
				if(this.imageIndex == 0) {
					this.imageSpeed = 0;
				}
			}
			if(this.x + this.xSpeed < 0) {
				this.x = 0;
				this.xSpeed = -this.xSpeed;
			}
			if(this.x + this.xSpeed > room.width - this.width) {
				this.x = room.width - this.width;
				this.xSpeed = -this.xSpeed;
			}
			if(this.y + this.ySpeed < 0) {
				this.y = 0;
				this.ySpeed = -this.ySpeed;
			}
			if(this.y + this.ySpeed > room.height - this.height) {
				this.y = room.height - this.height;
				this.ySpeed = -this.ySpeed;
			}
		}
		return SHIP_INSTANCE;
	}

	function Asteroid(room) {
		var ASTEROID_INSTANCE = new BlowFishObject2D(
			R.getSprite(R.SPRITE_ASTEROID_1));
		ASTEROID_INSTANCE.scale = Math.random()/2;
		ASTEROID_INSTANCE.x = Math.random() * (room.width - (ASTEROID_INSTANCE.width * ASTEROID_INSTANCE.scale));
		ASTEROID_INSTANCE.y = Math.random() * (room.width - (ASTEROID_INSTANCE.width * ASTEROID_INSTANCE.scale));
		ASTEROID_INSTANCE.z = -Math.floor(5000 * Math.random()) + -4999;
		ASTEROID_INSTANCE.xSpeed = Math.random() * 5;
		if(Math.random() > .5) {
			ASTEROID_INSTANCE.xSpeed = -ASTEROID_INSTANCE.xSpeed;
		}
		ASTEROID_INSTANCE.ySpeed = Math.random() * 5;
		if(Math.random() > .5) {
			ASTEROID_INSTANCE.ySpeed = -ASTEROID_INSTANCE.ySpeed;
		}
		ASTEROID_INSTANCE.rSpeed = Math.random()/10;
		if(Math.random() > .5) {
			ASTEROID_INSTANCE.rSpeed = -ASTEROID_INSTANCE.rSpeed;
		}
		ASTEROID_INSTANCE.imageSpeed = Math.random()/2 + .5;

		ASTEROID_INSTANCE.onUpdate = function() {
			var room = BLOWFISH_INSTANCE.get2DRoom();
			if(this.x > room.width + BLOWFISH_INSTANCE.getCanvasWidth()) {
				this.x = room.x - BLOWFISH_INSTANCE.getCanvasWidth();
			}
			else if(this.x < room.x - BLOWFISH_INSTANCE.getCanvasWidth()) {
				this.x = room.width + BLOWFISH_INSTANCE.getCanvasWidth();
			}
			if(this.y > room.height + BLOWFISH_INSTANCE.getCanvasHeight()) {
				this.y = room.y - BLOWFISH_INSTANCE.getCanvasHeight();
			}
			else if(this.y < room.y - BLOWFISH_INSTANCE.getCanvasHeight()) {
				this.y = room.height + BLOWFISH_INSTANCE.getCanvasHeight();
			}
		}

		return ASTEROID_INSTANCE;
	}

	function PrototypeLevel() {
		var PROTOTYPE_LEVEL_INSTANCE 
			= new BlowFishRoom2D(8530, 4800);

		PROTOTYPE_LEVEL_INSTANCE.onDraw = function(BlowFishG2D) {
			BlowFishG2D.fillStyle = "#110011";
			BlowFishG2D.fillRect(0, 0, BlowFishG2D.getRoom().width, BlowFishG2D.getRoom().height);
			var player = BlowFishG2D.getRoom().getObjectsByClassName("Player")[0];
			var sprite = GLOBAL_R.getSprite(GLOBAL_R.SPRITE_BACKGROUND_1);

			//Background
			var offsetX = (player.x + (player.width/2)) * .8;
			var offsetY = (player.y + (player.height/2)) * .8;
			for(var i=0;i<2;i+=1) {
				for(var j=0;j<2;j+=1) {
					BlowFishG2D.drawImage(sprite[0], offsetX + this.x + (i * sprite[0].width), offsetY + this.y + (j * sprite[0].height));
				}
			}
			//Midground
			offsetX = (player.x + (player.width/2)) * .6;
			offsetY = (player.y + (player.height/2)) * .6;
			for(var i=0;i<4;i+=1) {
				for(var j=0;j<4;j+=1) {
					BlowFishG2D.drawImage(sprite[1], offsetX + this.x + (i * sprite[1].width), offsetY + this.y + (j * sprite[1].height));
				}
			}
			//Foreground
			offsetX = (player.x + (player.width/2)) * .4;
			offsetY = (player.y + (player.height/2)) * .4;
			for(var i=0;i<6;i+=1) {
				for(var j=0;j<6;j+=1) {
					BlowFishG2D.drawImage(sprite[2], offsetX + this.x + (i * sprite[2].width), offsetY + this.y + (j * sprite[2].height));
				}
			}
			BlowFishG2D.fillStyle = "#ff0000";
			BlowFishG2D.fillText("FPS: " 
				+ BLOWFISH_INSTANCE.getFPS(), 250, 50);
		}

		var p = new Player(2000, 750);
		PROTOTYPE_LEVEL_INSTANCE.x -= p.x - ((BLOWFISH_INSTANCE.getCanvasWidth() - p.width)/2);
		PROTOTYPE_LEVEL_INSTANCE.y -= p.y - ((BLOWFISH_INSTANCE.getCanvasHeight() - p.height)/2);

		for(var i=0;i<25;i+=1) {PROTOTYPE_LEVEL_INSTANCE.add(new Asteroid(PROTOTYPE_LEVEL_INSTANCE))}
		PROTOTYPE_LEVEL_INSTANCE.add(p);
		PROTOTYPE_LEVEL_INSTANCE.add(new BattleInterface());
		R.getSound(R.SOUND_SONG1).play();

		return PROTOTYPE_LEVEL_INSTANCE;
	}

	function BattleInterface() {
		var BATTLE_INTERFACE_INSTANCE = new BlowFishObject2D();
		BATTLE_INTERFACE_INSTANCE.radar = new Radar();
		BATTLE_INTERFACE_INSTANCE.emblem = new Emblem();
		BATTLE_INTERFACE_INSTANCE.healthBar = new HealthBar();
		BATTLE_INTERFACE_INSTANCE.energyBar = new EnergyBar();
		BATTLE_INTERFACE_INSTANCE.controller = new OnScreenController();
		BATTLE_INTERFACE_INSTANCE.z = 9999;

		BATTLE_INTERFACE_INSTANCE.onDraw = function(BlowFishG2D) {
			this.radar.onDraw(BlowFishG2D);
			this.healthBar.onDraw(BlowFishG2D);
			this.energyBar.onDraw(BlowFishG2D);
			this.emblem.onDraw(BlowFishG2D);
			if(this.controller.visible) {
				this.controller.onDraw(BlowFishG2D);
			}
		}

		function OnScreenController() {
			var ONSCREEN_CONTROLLER_INSTANCE = this;
			ONSCREEN_CONTROLLER_INSTANCE.visible = true;
			ONSCREEN_CONTROLLER_INSTANCE.joystick = new JoyStick();
			ONSCREEN_CONTROLLER_INSTANCE.aButton = new Button(745, 445, "#00ff00", "A");
			ONSCREEN_CONTROLLER_INSTANCE.bButton = new Button(800, 387, "#ff0000", "B");
			ONSCREEN_CONTROLLER_INSTANCE.cButton = new Button(745, 330, "#ffff00", "C");
			ONSCREEN_CONTROLLER_INSTANCE.dButton = new Button(690, 387, "#0000ff", "D");

			ONSCREEN_CONTROLLER_INSTANCE.onDraw = function(BlowFishG2D) {
				BlowFishG2D.globalAlpha = 0.2;
				ONSCREEN_CONTROLLER_INSTANCE.joystick.onDraw(BlowFishG2D);
				ONSCREEN_CONTROLLER_INSTANCE.aButton.onDraw(BlowFishG2D);
				ONSCREEN_CONTROLLER_INSTANCE.bButton.onDraw(BlowFishG2D);
				ONSCREEN_CONTROLLER_INSTANCE.cButton.onDraw(BlowFishG2D);
				ONSCREEN_CONTROLLER_INSTANCE.dButton.onDraw(BlowFishG2D);
				BlowFishG2D.globalAlpha = 1.0;
			}

			function JoyStick() {
				var JOYSTICK_INSTANCE = this;

				JOYSTICK_INSTANCE.onDraw = function(BlowFishG2D) {
					BlowFishG2D.save();
					BlowFishG2D.fillStyle = "#ffffff";
					BlowFishG2D.beginPath();
					BlowFishG2D.arc(103, 385, 75, 0, 2 * Math.PI);
					BlowFishG2D.fill();
					BlowFishG2D.closePath();
					BlowFishG2D.beginPath();
					BlowFishG2D.fillStyle = "#222222";
					BlowFishG2D.arc(103, 385, 60, 0, 2 * Math.PI);
					BlowFishG2D.fill();
					BlowFishG2D.closePath();
					BlowFishG2D.restore();
				}

				return JOYSTICK_INSTANCE;
			}

			function Button(x, y, color, text) {
				var BUTTON_INSTANCE = this;
				BUTTON_INSTANCE.x = x;
				BUTTON_INSTANCE.y = y;
				BUTTON_INSTANCE.onDraw = function(BlowFishG2D) {
					BlowFishG2D.save();
					BlowFishG2D.fillStyle = color;
					BlowFishG2D.beginPath();
					BlowFishG2D.arc(this.x, this.y, 30, 0, 2 * Math.PI);
					BlowFishG2D.fill();
					BlowFishG2D.closePath();
					BlowFishG2D.fillStyle = "#ffffff";
					BlowFishG2D.fillText(text, this.x - 13, this.y + 14);
					BlowFishG2D.restore();
				}
				return BUTTON_INSTANCE;
			}

			return ONSCREEN_CONTROLLER_INSTANCE;
		}

		function HealthBar() {
			var HEALTH_BAR_INSTANCE = this;
			HEALTH_BAR_INSTANCE.x = 220;
			HEALTH_BAR_INSTANCE.y = 435;
			HEALTH_BAR_INSTANCE.width = 200;
			HEALTH_BAR_INSTANCE.height = 25;
			HEALTH_BAR_INSTANCE.onDraw = function(BlowFishG2D) {
				BlowFishG2D.save();
				BlowFishG2D.globalAlpha = 0.5;
				BlowFishG2D.fillStyle = "#ffffff";
				BlowFishG2D.beginPath();
				BlowFishG2D.arc(this.x, this.y + (this.height/2), 12.5, 0.5 * Math.PI, 1.5 * Math.PI);
				BlowFishG2D.moveTo(this.x, this.y);
				BlowFishG2D.lineTo(this.x + this.width, this.y);
				BlowFishG2D.lineTo(this.x + this.width, this.y + this.height);
				BlowFishG2D.lineTo(this.x, this.y + this.height);
				BlowFishG2D.fill();
				BlowFishG2D.closePath();
				BlowFishG2D.globalAlpha = 1.0;
				BlowFishG2D.fillStyle = "#ff0000";
				BlowFishG2D.beginPath();
				BlowFishG2D.arc(this.x, this.y + (this.height/2), 12.5, 0.5 * Math.PI, 1.5 * Math.PI);
				BlowFishG2D.moveTo(this.x, this.y);
				BlowFishG2D.lineTo(this.x + this.width, this.y);
				BlowFishG2D.lineTo(this.x + this.width, this.y + this.height);
				BlowFishG2D.lineTo(this.x, this.y + this.height);
				BlowFishG2D.fill();
				BlowFishG2D.closePath();
				BlowFishG2D.restore();
			}
			return HEALTH_BAR_INSTANCE;
		}

		function EnergyBar() {
			var ENERGY_BAR_INSTANCE = this;
			ENERGY_BAR_INSTANCE.width = 200;
			ENERGY_BAR_INSTANCE.height = 25;
			ENERGY_BAR_INSTANCE.x = BLOWFISH_INSTANCE.getCanvasWidth() - 220 - ENERGY_BAR_INSTANCE.width;
			ENERGY_BAR_INSTANCE.y = 435;
			ENERGY_BAR_INSTANCE.onDraw = function(BlowFishG2D) {
				BlowFishG2D.save();
				BlowFishG2D.globalAlpha = 0.5;
				BlowFishG2D.fillStyle = "#ffffff";
				BlowFishG2D.beginPath();
				BlowFishG2D.arc(this.x + this.width, this.y + (this.height/2), 12.5, 0.5 * Math.PI, 1.5 * Math.PI, true);
				BlowFishG2D.lineTo(this.x, this.y);
				BlowFishG2D.lineTo(this.x, this.y + this.height);
				BlowFishG2D.lineTo(this.x + this.width, this.y + this.height);
				BlowFishG2D.fill();
				BlowFishG2D.closePath();
				BlowFishG2D.globalAlpha = 1.0;
				BlowFishG2D.fillStyle = "#0000ff";
				BlowFishG2D.beginPath();
				BlowFishG2D.arc(this.x + this.width, this.y + (this.height/2), 12.5, 0.5 * Math.PI, 1.5 * Math.PI, true);
				BlowFishG2D.lineTo(this.x, this.y);
				BlowFishG2D.lineTo(this.x, this.y + this.height);
				BlowFishG2D.lineTo(this.x + this.width, this.y + this.height);
				BlowFishG2D.fill();
				BlowFishG2D.closePath();
				BlowFishG2D.restore();
			}
			return ENERGY_BAR_INSTANCE;
		}

		function Emblem() {
			var EMBLEM_INSTANCE = this;
			EMBLEM_INSTANCE.x = (BLOWFISH_INSTANCE.getCanvasWidth()/2) - (R.getSprite(R.SPRITE_EMBLEM)[0].width/2);
			EMBLEM_INSTANCE.y = 410;
			EMBLEM_INSTANCE.onDraw = function(BlowFishG2D) {
				BlowFishG2D.drawImage(R.getSprite(R.SPRITE_EMBLEM)[0], this.x, this.y);
			}
			return EMBLEM_INSTANCE;
		}

		function Radar() {
			var RADAR_INSTANCE = this;
			RADAR_INSTANCE.onDraw = function(BlowFishG2D) {
				BlowFishG2D.save();
				BlowFishG2D.globalAlpha = .5;
				BlowFishG2D.beginPath();
				BlowFishG2D.arc(100,100,80,0,2*Math.PI);
				BlowFishG2D.fillStyle = "#ffffff";
				BlowFishG2D.fill();
				BlowFishG2D.closePath();
				BlowFishG2D.beginPath();
				BlowFishG2D.arc(100,100,75,0,2*Math.PI);
				BlowFishG2D.fillStyle = "#000000";
				BlowFishG2D.fill();
				BlowFishG2D.restore();
			}
			return RADAR_INSTANCE;
		}
		return BATTLE_INTERFACE_INSTANCE;
	}

	function MainMenu() {
		var MAIN_MENU_INSTANCE = new BlowFishRoom2D(853, 480);

		MAIN_MENU_INSTANCE.onDraw = function(BlowFishG2D) {
			BlowFishG2D.fillStyle = "#110011";
			BlowFishG2D.fillRect(0, 0, BlowFishG2D.getRoom().width, BlowFishG2D.getRoom().height);
			var ship = BlowFishG2D.getRoom().getObjectsByClassName("Ship")[0];
			var sprite = GLOBAL_R.getSprite(GLOBAL_R.SPRITE_BACKGROUND_1);
			//Background
			var offsetX = (ship.x + (ship.width/2)) * .8;
			var offsetY = (ship.y + (ship.height/2)) * .8;
			for(var i=0;i<2;i+=1) {
				for(var j=0;j<2;j+=1) {
					BlowFishG2D.drawImage(sprite[0], offsetX + this.x + (i * sprite[0].width), offsetY + this.y + (j * sprite[0].height));
				}
			}
			//Midground
			offsetX = (ship.x + (ship.width/2)) * .6;
			offsetY = (ship.y + (ship.height/2)) * .6;
			for(var i=0;i<4;i+=1) {
				for(var j=0;j<4;j+=1) {
					BlowFishG2D.drawImage(sprite[1], offsetX + this.x + (i * sprite[1].width), offsetY + this.y + (j * sprite[1].height));
				}
			}
			//Foreground
			offsetX = (ship.x + (ship.width/2)) * .4;
			offsetY = (ship.y + (ship.height/2)) * .4;
			for(var i=0;i<6;i+=1) {
				for(var j=0;j<6;j+=1) {
					BlowFishG2D.drawImage(sprite[2], offsetX + this.x + (i * sprite[2].width), offsetY + this.y + (j * sprite[2].height));
				}
			}
		}
		var ship = new Ship();
		ship.xSpeed = 5;
		ship.ySpeed = 5;
		MAIN_MENU_INSTANCE.add(ship);

		return MAIN_MENU_INSTANCE;
	}

	function LoadingScreen() {
		var LOADING_SCREEN_INSTANCE = new BlowFishRoom2D(853, 480);
		LOADING_SCREEN_INSTANCE.progress = 0;
		LOADING_SCREEN_INSTANCE.onDraw = function(BlowFishG2D) {
			BlowFishG2D.fillStyle = "#110011";
			BlowFishG2D.fillRect(0, 0, BlowFishG2D.getRoom().width, BlowFishG2D.getRoom().height);
			var ship = BlowFishG2D.getRoom().getObjectsByClassName("Ship")[0];
			var sprite = GLOBAL_R.getSprite(GLOBAL_R.SPRITE_BACKGROUND_1);
			//Background
			var offsetX = (ship.x + (ship.width/2)) * .8;
			var offsetY = (ship.y + (ship.height/2)) * .8;
			for(var i=0;i<2;i+=1) {
				for(var j=0;j<2;j+=1) {
					BlowFishG2D.drawImage(sprite[0], offsetX + this.x + (i * sprite[0].width), offsetY + this.y + (j * sprite[0].height));
				}
			}
			//Midground
			offsetX = (ship.x + (ship.width/2)) * .6;
			offsetY = (ship.y + (ship.height/2)) * .6;
			for(var i=0;i<4;i+=1) {
				for(var j=0;j<4;j+=1) {
					BlowFishG2D.drawImage(sprite[1], offsetX + this.x + (i * sprite[1].width), offsetY + this.y + (j * sprite[1].height));
				}
			}
			//Foreground
			offsetX = (ship.x + (ship.width/2)) * .4;
			offsetY = (ship.y + (ship.height/2)) * .4;
			for(var i=0;i<6;i+=1) {
				for(var j=0;j<6;j+=1) {
					BlowFishG2D.drawImage(sprite[2], offsetX + this.x + (i * sprite[2].width), offsetY + this.y + (j * sprite[2].height));
				}
			}
			BlowFishG2D.fillStyle = "#ffffff";
			BlowFishG2D.font = "Bold 32pt 'Lucida Console', Monaco, monospace";
			BlowFishG2D.fillText("Loading...", 30, 50);
			BlowFishG2D.fillRect(50, 250, this.width - 100, 25);
			BlowFishG2D.fillText(this.progress + "%", this.width - 150, this.height - 20);
			BlowFishG2D.fillStyle = "#00ff00";
			BlowFishG2D.fillRect(50, 250, (this.progress/100) * (this.width - 100), 25);
		}
		var ship = new Ship();
		ship.xSpeed = 2;
		ship.ySpeed = 2;
		LOADING_SCREEN_INSTANCE.add(ship);
		return LOADING_SCREEN_INSTANCE;
	}

	return UNKNOWNSPACE_INSTANCE;
}